from sys import argv

files = map(lambda f: open(f,'r'),argv[1:])
lines = map(lambda f: f.readlines(),files)
map(lambda f: f.close(),files)

nfile = len(lines)
nline = len(lines[0])
for iline in range(nline):
  try:
    agg = sum(float(lines[i][iline]) for i in range(nfile))
    print('%21.12E' % agg)
  except: print(lines[0][iline][:-1])

