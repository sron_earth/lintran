program lintran_standalone

  use lintran_constants_module, only: nst_max, nscat_nst, solver_gauss_seidel, ist_u
  use lintran_types_module, only: lintran_atmosphere, lintran_settings, lintran_derivatives
  use lintran_module, only: lintran_class, lintran_init, lintran_provide, lintran_calculate, lintran_close

  implicit none

  ! Run identifier from argument.
  character(len=100) :: argument
  integer :: runid
  character(len=16) :: namelist_filename
  character(len=15) :: result_filename
  character(len=18) :: result_fd_filename
  character(len=23) :: result_fdsmall_filename
  integer, parameter :: asciiunit_namelist = 20
  integer, parameter :: asciiunit_result = 30
  integer, parameter :: asciiunit_result_fd = 40
  integer, parameter :: asciiunit_result_fdsmall = 50
  integer :: asciiunit

  ! Namelist settings.

  ! General.
  integer :: nst
  integer :: streams
  logical, dimension(3) :: execution
  logical :: execute_differentiation
  character(len=100) :: atmosphere_filename
  logical :: finite_difference
  real :: perturbation

  ! Geometry.
  real :: sza
  integer :: ngeo
  real, dimension(10) :: vza
  real, dimension(10) :: raa
  logical :: derive_phase_ssg
  real :: lambert_albedo
  real :: uniform_emissivity
  logical :: disable_thermal_emission
  logical :: pseudo_spherical

  ! Settings.
  logical, dimension(nst_max) :: execute_stokes
  logical, dimension(nst_max) :: differentiate_stokes
  real :: taus_split
  real :: taua_split
  real :: tautot_max
  real :: fourier_tolerance
  logical :: split_double
  integer :: interpolation
  logical :: deltam
  integer :: solver
  integer :: gs_maxiter
  real :: gs_tolerance

  ! Differentiation.
  integer :: nleg_deriv
  integer, dimension(200) :: ilay_deriv_base
  integer, dimension(200) :: ilay_deriv_ph

  ! Derived dimension sizes.
  integer :: nscat
  integer :: nstrhalf
  integer :: nleg
  integer :: nlay
  integer :: nlay_deriv_base
  integer :: nlay_deriv_ph

  ! Parameters for pseudo-spherical geometry.
  real, dimension(:), allocatable :: zlev
  real, parameter :: radius = 6.371e6 ! Earth radius [m].

  integer :: ierr ! Error handling.

  ! The Lintran input instances.
  type(lintran_atmosphere) :: atm ! The atmosphere.
  type(lintran_settings) :: set ! The settings.
  ! The Lintran output instances.
  type(lintran_derivatives), target :: drv ! Derivatives.
  real, dimension(:,:), allocatable :: rint ! Main output.

  ! Finite difference.
  type(lintran_derivatives), target :: drv_fd ! Finite-difference.
  type(lintran_derivatives), target :: drv_fdsmall ! Numeric check: ten times as small difference.

  ! Writing files.
  type(lintran_derivatives), pointer :: drv_write ! Pointer to relevant derivatives instance.
  integer :: nfile ! Number of output files.

  ! Geometry.
  real :: mu_0 ! Cosine of solar zenith angle.
  real, dimension(:), allocatable :: mu_v ! Cosine of viewing zenith angles.
  real, dimension(:), allocatable :: azi ! Aximuth angles in radians.

  ! The Lintran instance.
  type(lintran_class) :: lintran_instance ! This is where all stuff is happening.
  ! The Lintran instance is intent out in lintran_init and intent inout everywhere else.

  ! Iterators.
  integer :: ifile ! Over output files.
  integer :: ilay ! Over layers.
  integer :: ileg ! Over Legendre coefficients.
  integer :: igeo ! Over viewing geometries.
  integer :: istr ! Streams.
  integer :: istr2 ! More streams.
  integer :: ist ! Stokes parameters.
  integer :: ist2 ! More Stokes parameters.
  integer :: ideriv ! Over differentiated layers.
  integer :: iscat ! Over independent matrix elements.

  ! CPU timing (for optimization).
  real :: time_ini
  real :: time_fin

  ! Unit.
  real, parameter :: degrees = acos(0.0) / 90.0

  ! Stuff for finite difference.
  real, dimension(:,:), allocatable :: rint_pert ! Result of perturbed run.
  real, dimension(:,:), allocatable :: rint_pertsmall ! Result of perturbed run with smaller perturbation.
  real :: orig ! Save the unperturbed value.
  real, dimension(:), allocatable :: orig_geo ! Save the unperturbed values for different geometries.

  ! Error handling.
  integer :: stat

  namelist/general/nst,streams,execution,execute_differentiation,atmosphere_filename,finite_difference,perturbation
  namelist/geometry/ngeo,sza,vza,raa,derive_phase_ssg,lambert_albedo,uniform_emissivity,disable_thermal_emission,pseudo_spherical
  namelist/settings/execute_stokes,differentiate_stokes,taus_split,taua_split,tautot_max,fourier_tolerance,split_double,interpolation,deltam,solver,gs_maxiter,gs_tolerance
  namelist/differentiation/nleg_deriv,ilay_deriv_base,ilay_deriv_ph

  ! Read the namelist. {{{
  nst = 0
  streams = 0
  execution = (/.false.,.false.,.false./)
  execute_differentiation = .false.
  atmosphere_filename = ""
  finite_difference = .false.
  perturbation = 0.0
  ngeo = 0
  sza = 0.0
  vza = 0.0
  raa = 0.0
  derive_phase_ssg = .false.
  lambert_albedo = -1.0
  uniform_emissivity = -1.0
  disable_thermal_emission = .false.
  pseudo_spherical = .false.
  execute_stokes = (/.false.,.false.,.false.,.false./)
  differentiate_stokes = (/.false.,.false.,.false.,.false./)
  taus_split = 0.0
  taua_split = 0.0
  tautot_max = 0.0
  fourier_tolerance = -1.0
  split_double = .false.
  interpolation = 0
  solver = 0
  gs_maxiter = 0
  gs_tolerance = -1.0
  nleg_deriv = 0
  ilay_deriv_base = 0
  ilay_deriv_ph = 0
  if (iargc() .lt. 1) then
    write(6,"(A)") "Give run ID as argument."
    stop
  endif
  call getarg(1,argument)
  read(argument,"(I4)") runid
  write(namelist_filename,"(A,I4.4,A)") "settings",runid,".nml"
  open(asciiunit_namelist,file=namelist_filename,iostat=ierr)
  if (ierr .ne. 0) then
    write(6,"(A)") "Error: Namelist file "//namelist_filename//" not found."
    stop
  endif
  read(asciiunit_namelist,general,iostat=ierr)
  if (ierr .ne. 0) then
    write(6,"(A)") "Error reading tag 'general' in namelist file."
    stop
  endif
  rewind(asciiunit_namelist)
  read(asciiunit_namelist,geometry,iostat=ierr)
  if (ierr .ne. 0) then
    write(6,"(A)") "Error reading tag 'geometry' in namelist file."
    stop
  endif
  rewind(asciiunit_namelist)
  read(asciiunit_namelist,settings,iostat=ierr)
  if (ierr .ne. 0) then
    write(6,"(A)") "Error reading tag 'settings' in namelist file."
    stop
  endif
  rewind(asciiunit_namelist)
  read(asciiunit_namelist,differentiation,iostat=ierr)
  if (ierr .ne. 0) then
    write(6,"(A)") "Error reading tag 'differentiation' in namelist file."
    stop
  endif
  rewind(asciiunit_namelist)
  close(asciiunit_namelist)
  ! }}}

  ! Check if the input makes sense. {{{
  if (nst .ne. 1 .and. nst .ne. 3 .and. nst .ne. 4) then
    write(6,"(A)") "Error: Number of Stokes parameters makes no sense."
    stop
  endif
  if (streams .lt. 2 .or. modulo(streams,2) .eq. 1) then
    write(6,"(A)") "Error: Number of streams makes no sense."
    stop
  endif
  if (finite_difference .and. .not. execute_differentiation) then
    write(6,"(A)") "Error: Needs differentiation to compare with finite difference."
    stop
  endif
  if (finite_difference .and. perturbation .eq. 0.0) then
    write(6,"(A)") "Error: perturbation makes no sense."
    stop
  endif
  if (ngeo .lt. 0) then
    write(6,"(A)") "Error: Number of viewing geometries makes no sense."
    stop
  endif
  if (taus_split .le. 0.0) then
    write(6,"(A)") "Error: Scattering split criterion makes no sense."
    stop
  endif
  if (taua_split .le. 0.0) then
    write(6,"(A)") "Error: Absorption split criterion makes no sense."
    stop
  endif
  if (tautot_max .le. 0.0) then
    write(6,"(A)") "Error: Maximum optical depth for splitting makes no sense."
    stop
  endif
  if (fourier_tolerance .lt. 0.0) then
    write(6,"(A)") "Error: Fourier convergence criterion makes no sense."
    stop
  endif
  if (interpolation .lt. 1 .or. interpolation .gt. 2) then
    write(6,"(A)") "Error: Interpolation scheme makes no sense."
    stop
  endif
  if (solver .lt. 1 .or. solver .gt. 2) then
    write(6,"(A)") "Error: Matrix solver makes no sense."
    stop
  endif
  if (solver .eq. solver_gauss_seidel .and. gs_maxiter .lt. 1) then
    write(6,"(A)") "Error: Number of Gauss-Seidel iterations makes no sense."
    stop
  endif
  if (solver .eq. solver_gauss_seidel .and. gs_tolerance .lt. 0.0) then
    write(6,"(A)") "Error: Gauss-Seidel convergence criterion makes no sense."
    stop
  endif
  ! }}}

  ! Execution 2 is empty for non-split-off double scattering. Not necessary to do this for
  ! Lintran, but we also use the execution flags inside this main.
  if (.not. split_double) execution(2) = .false.

  ! Put the settings in the structure.
  set%taus_split = taus_split
  set%taua_split = taua_split
  set%tautot_max = tautot_max
  set%fourier_tolerance = fourier_tolerance
  set%split_double = split_double
  set%interpolation = interpolation
  set%deltam = deltam
  set%solver = solver
  set%gs_maxiter = gs_maxiter
  set%gs_tolerance = gs_tolerance

  ! Read the atmosphere.
  call atmosphere_read(trim(atmosphere_filename),atm)

  ! Create the geometry.
  allocate(mu_v(ngeo))
  allocate(azi(ngeo))
  mu_0 = cos(sza * degrees)
  mu_v = cos(vza(1:ngeo) * degrees)
  azi = raa(1:ngeo) * degrees

  if (derive_phase_ssg) call atmosphere_derive_phase_ssg(ngeo,mu_0,mu_v,azi,atm)

  ! Derive the minimum number of neccessary independent matrix elements
  ! for a calculation with this number of Stokes parameters.
  nscat = nscat_nst(nst) ! For the derivatives.
  ! Derive the most important dimension size ever.
  nstrhalf = streams / 2 ! Half the number of streams.
  nleg = streams - 1 ! Number of Legendre moments, as Fourier moments.

  ! Replace old BDRF with a Lambertian one. This may increase or
  ! decrease the number of streams. This is also true for the uniform
  ! emissivity. If only one of them is set, you cannot alter the number
  ! of streams.
  if (lambert_albedo .ne. -1.0) call atmosphere_lambert_albedo(nst,nstrhalf,ngeo,execution,lambert_albedo,atm)
  if (uniform_emissivity .ne. -1.0) call atmosphere_uniform_emissitivy(nst,nstrhalf,ngeo,execution,uniform_emissivity,atm)
  if (disable_thermal_emission) atm%thermal_emission = .false.

  nlay = size(atm%taus) ! The rest must be consistent.

  ! Stokes parameters to execute.
  allocate(set%execute_stokes(nst))
  set%execute_stokes = execute_stokes(1:nst)

  ! The end results.
  allocate(rint(nst,ngeo))
  if (execute_differentiation) then

    ! Stokes parameters to differentiate,
    allocate(set%differentiate_stokes(nst))
    set%differentiate_stokes = differentiate_stokes(1:nst)

    ! Find out which layers to differentiate. {{{
    if (any(ilay_deriv_base .lt. 0)) then
      nlay_deriv_base = nlay
    else
      nlay_deriv_base = count(ilay_deriv_base .gt. 0 .and. ilay_deriv_base .le. nlay)
    endif
    allocate(set%ilay_deriv_base(nlay_deriv_base))
    if (any(ilay_deriv_base .lt. 0)) then
      set%ilay_deriv_base = (/(ilay,ilay=1,nlay)/)
    else
      set%ilay_deriv_base = pack(ilay_deriv_base,ilay_deriv_base .gt. 0 .and. ilay_deriv_base .le. nlay)
    endif
    if (any(ilay_deriv_ph .lt. 0)) then
      nlay_deriv_ph = nlay
    else
      nlay_deriv_ph = count(ilay_deriv_ph .gt. 0 .and. ilay_deriv_ph .le. nlay)
    endif
    allocate(set%ilay_deriv_ph(nlay_deriv_ph))
    if (any(ilay_deriv_ph .lt. 0)) then
      set%ilay_deriv_ph = (/(ilay,ilay=1,nlay)/)
    else
      set%ilay_deriv_ph = pack(ilay_deriv_ph,ilay_deriv_ph .gt. 0 .and. ilay_deriv_ph .le. nlay)
    endif
    ! }}}
    ! Save one forgotten number.
    set%nleg_deriv = nleg_deriv
    ! Allocate the derivatives, also those that are not needed.
    allocate(drv%drint_taus(nlay_deriv_base,nst,ngeo))
    allocate(drv%drint_taua(nlay_deriv_base,nst,ngeo))
    allocate(drv%drint_coefs(nscat,0:nleg_deriv,nlay_deriv_ph,nst,ngeo))
    allocate(drv%drint_phase_ssg(nscat,nlay_deriv_ph,nst,ngeo))
    allocate(drv%drint_bdrf(nst,nst,nstrhalf,nstrhalf,0:nleg,nst,ngeo))
    allocate(drv%drint_bdrf_0(nst,nst,nstrhalf,0:nleg,nst,ngeo))
    allocate(drv%drint_bdrf_v(nst,nst,nstrhalf,0:nleg,nst,ngeo))
    allocate(drv%drint_bdrf_ssg(nst,nst,nst,ngeo))
    allocate(drv%drint_emi(nst,nstrhalf,0:nleg,nst,ngeo))
    allocate(drv%drint_emi_ssg(nst,nst,ngeo))
    allocate(drv%drint_planck_curve(nlay,nst,ngeo))
    if (finite_difference) then
      allocate(drv_fd%drint_taus(nlay_deriv_base,nst,ngeo))
      allocate(drv_fd%drint_taua(nlay_deriv_base,nst,ngeo))
      allocate(drv_fd%drint_coefs(nscat,0:nleg_deriv,nlay_deriv_ph,nst,ngeo))
      allocate(drv_fd%drint_phase_ssg(nscat,nlay_deriv_ph,nst,ngeo))
      allocate(drv_fd%drint_bdrf(nst,nst,nstrhalf,nstrhalf,0:nleg,nst,ngeo))
      allocate(drv_fd%drint_bdrf_0(nst,nst,nstrhalf,0:nleg,nst,ngeo))
      allocate(drv_fd%drint_bdrf_v(nst,nst,nstrhalf,0:nleg,nst,ngeo))
      allocate(drv_fd%drint_bdrf_ssg(nst,nst,nst,ngeo))
      allocate(drv_fd%drint_emi(nst,nstrhalf,0:nleg,nst,ngeo))
      allocate(drv_fd%drint_emi_ssg(nst,nst,ngeo))
      allocate(drv_fd%drint_planck_curve(nlay,nst,ngeo))
      allocate(drv_fdsmall%drint_taus(nlay_deriv_base,nst,ngeo))
      allocate(drv_fdsmall%drint_taua(nlay_deriv_base,nst,ngeo))
      allocate(drv_fdsmall%drint_coefs(nscat,0:nleg_deriv,nlay_deriv_ph,nst,ngeo))
      allocate(drv_fdsmall%drint_phase_ssg(nscat,nlay_deriv_ph,nst,ngeo))
      allocate(drv_fdsmall%drint_bdrf(nst,nst,nstrhalf,nstrhalf,0:nleg,nst,ngeo))
      allocate(drv_fdsmall%drint_bdrf_0(nst,nst,nstrhalf,0:nleg,nst,ngeo))
      allocate(drv_fdsmall%drint_bdrf_v(nst,nst,nstrhalf,0:nleg,nst,ngeo))
      allocate(drv_fdsmall%drint_bdrf_ssg(nst,nst,nst,ngeo))
      allocate(drv_fdsmall%drint_emi(nst,nstrhalf,0:nleg,nst,ngeo))
      allocate(drv_fdsmall%drint_emi_ssg(nst,nst,ngeo))
      allocate(drv_fdsmall%drint_planck_curve(nlay,nst,ngeo))
    endif
  endif

  ! This is now still hardcoded.
  allocate(zlev(0:nlay))
  zlev = (/(real(ilay) * 1000.0, ilay = nlay,0,-1)/)

  ! Set up calculation.
  call lintran_init(nst,streams,nlay,ngeo,execute_differentiation,lintran_instance,stat)
  call lintran_provide(mu_0,mu_v,azi,pseudo_spherical,radius,zlev,lintran_instance,stat)

  call cpu_time(time_ini)
  if (execute_differentiation) then
    call lintran_calculate(execution,atm,set,lintran_instance,rint,drv,stat)
  else
    call lintran_calculate(execution,atm,set,lintran_instance,rint,stat)
  endif
  call cpu_time(time_fin)

  ! CPU time is only for one run with derivatives, not for init, provide or printing.

  write(result_filename,"(A,I4.4,A)") "results",runid,".txt"
  open(asciiunit_result,file=result_filename)
  if (execute_differentiation) then

    if (finite_difference) then

      nfile = 3

      ! Finite difference. {{{

      write(result_fd_filename,"(A,I4.4,A)") "results_fd",runid,".txt"
      open(asciiunit_result_fd,file=result_fd_filename)
      write(result_fdsmall_filename,"(A,I4.4,A)") "results_fdsmall",runid,".txt"
      open(asciiunit_result_fdsmall,file=result_fdsmall_filename)

      allocate(rint_pert(nst,ngeo))
      allocate(rint_pertsmall(nst,ngeo))
      allocate(orig_geo(ngeo))

      ! Scattering optical depth.
      write(6,"(A)") "Scattering optical depth"
      do ideriv = 1,nlay_deriv_base

        ilay = set%ilay_deriv_base(ideriv)
        orig = atm%taus(ilay)

        atm%taus(ilay) = orig + 0.1 * perturbation
        call lintran_calculate(execution,atm,set,lintran_instance,rint_pertsmall,stat)
        atm%taus(ilay) = orig + perturbation
        call lintran_calculate(execution,atm,set,lintran_instance,rint_pert,stat)

        drv_fd%drint_taus(ideriv,:,:) = (rint_pert - rint) / perturbation
        drv_fdsmall%drint_taus(ideriv,:,:) = 10.0*(rint_pertsmall - rint) / perturbation

        atm%taus(ilay) = orig

      enddo

      ! Absorption optical depth.
      write(6,"(A)") "Absorption optical depth"
      do ideriv = 1,nlay_deriv_base

        ilay = set%ilay_deriv_base(ideriv)
        orig = atm%taua(ilay)

        atm%taua(ilay) = orig + 0.1 * perturbation
        call lintran_calculate(execution,atm,set,lintran_instance,rint_pertsmall,stat)
        atm%taua(ilay) = orig + perturbation
        call lintran_calculate(execution,atm,set,lintran_instance,rint_pert,stat)

        drv_fd%drint_taua(ideriv,:,:) = (rint_pert - rint) / perturbation
        drv_fdsmall%drint_taua(ideriv,:,:) = 10.0*(rint_pertsmall - rint) / perturbation

        atm%taua(ilay) = orig

      enddo

      ! Phase coefficients.
      write(6,"(A)") "Phase coefficients"
      do ideriv = 1,nlay_deriv_ph

        ilay = set%ilay_deriv_ph(ideriv)
        write(6,"(A,I4)") "Layer",ilay

        do ileg = 0,nleg_deriv

          do iscat = 1,nscat

            orig = atm%coefs(iscat,ileg,ilay)

            atm%coefs(iscat,ileg,ilay) = orig + 0.1 * perturbation
            call lintran_calculate(execution,atm,set,lintran_instance,rint_pertsmall,stat)
            atm%coefs(iscat,ileg,ilay) = orig + perturbation
            call lintran_calculate(execution,atm,set,lintran_instance,rint_pert,stat)

            drv_fd%drint_coefs(iscat,ileg,ideriv,:,:) = (rint_pert - rint) / perturbation
            drv_fdsmall%drint_coefs(iscat,ileg,ideriv,:,:) = 10.0*(rint_pertsmall - rint) / perturbation

            atm%coefs(iscat,ileg,ilay) = orig

          enddo

        enddo
      enddo

      ! Single-scattering phase function.
      write(6,"(A)") "Single-scattering phase function"
      do ideriv = 1,nlay_deriv_ph

        ilay = set%ilay_deriv_ph(ideriv)

        do iscat = 1,nscat

          orig_geo = atm%phase_ssg(iscat,ilay,:)

          atm%phase_ssg(iscat,ilay,:) = orig_geo + 0.1 * perturbation
          call lintran_calculate(execution,atm,set,lintran_instance,rint_pertsmall,stat)
          atm%phase_ssg(iscat,ilay,:) = orig_geo + perturbation
          call lintran_calculate(execution,atm,set,lintran_instance,rint_pert,stat)

          drv_fd%drint_phase_ssg(iscat,ideriv,:,:) = (rint_pert - rint) / perturbation
          drv_fdsmall%drint_phase_ssg(iscat,ideriv,:,:) = 10.0*(rint_pertsmall - rint) / perturbation

          atm%phase_ssg(iscat,ilay,:) = orig_geo

        enddo

      enddo

      ! Bi-directional reflection function, only internal streams.
      write(6,"(A)") "Bi-directional reflection function"
      do ileg = 0,nleg
        write(6,"(A,I4)") "Fourier number ",ileg
        do istr = 1,nstrhalf
          do istr2 = 1,istr
            do ist = 1,nst
              do ist2 = 1,nst
                if (istr .eq. istr2 .and. ist2 .gt. ist) cycle

                orig = atm%bdrf(ist2,ist,istr2,istr,ileg)

                atm%bdrf(ist2,ist,istr2,istr,ileg) = orig + 0.1 * perturbation
                if ((ist .eq. ist_u) .eqv. (ist2 .eq. ist_u)) then
                  atm%bdrf(ist,ist2,istr,istr2,ileg) = atm%bdrf(ist2,ist,istr2,istr,ileg)
                else
                  atm%bdrf(ist,ist2,istr,istr2,ileg) = -atm%bdrf(ist2,ist,istr2,istr,ileg)
                endif
                call lintran_calculate(execution,atm,set,lintran_instance,rint_pertsmall,stat)

                atm%bdrf(ist2,ist,istr2,istr,ileg) = orig + perturbation
                if ((ist .eq. ist_u) .eqv. (ist2 .eq. ist_u)) then
                  atm%bdrf(ist,ist2,istr,istr2,ileg) = atm%bdrf(ist2,ist,istr2,istr,ileg)
                else
                  atm%bdrf(ist,ist2,istr,istr2,ileg) = -atm%bdrf(ist2,ist,istr2,istr,ileg)
                endif
                call lintran_calculate(execution,atm,set,lintran_instance,rint_pert,stat)

                drv_fd%drint_bdrf(ist2,ist,istr2,istr,ileg,:,:) = (rint_pert - rint) / perturbation
                drv_fdsmall%drint_bdrf(ist2,ist,istr2,istr,ileg,:,:) = 10.0*(rint_pertsmall - rint) / perturbation
                if (istr2 .ne. istr .or. ist2 .ne. ist) then
                  drv_fd%drint_bdrf(ist,ist2,istr,istr2,ileg,:,:) = 0.0
                  drv_fdsmall%drint_bdrf(ist,ist2,istr,istr2,ileg,:,:) = 0.0
                endif

                atm%bdrf(ist2,ist,istr2,istr,ileg) = orig
                if ((ist .eq. ist_u) .eqv. (ist2 .eq. ist_u)) then
                  atm%bdrf(ist,ist2,istr,istr2,ileg) = atm%bdrf(ist2,ist,istr2,istr,ileg)
                else
                  atm%bdrf(ist,ist2,istr,istr2,ileg) = -atm%bdrf(ist2,ist,istr2,istr,ileg)
                endif

              enddo
            enddo
          enddo
        enddo
      enddo

      ! Bi-directional reflection function with solar direction.
      write(6,"(A)") "Bi-directional reflection function (sun)"
      do ileg = 0,nleg
        write(6,"(A,I4)") "Fourier number ",ileg
        do istr = 1,nstrhalf
          do ist = 1,nst
            do ist2 = 1,nst

              orig = atm%bdrf_0(ist2,ist,istr,ileg)

              atm%bdrf_0(ist2,ist,istr,ileg) = orig + 0.1 * perturbation
              call lintran_calculate(execution,atm,set,lintran_instance,rint_pertsmall,stat)

              atm%bdrf_0(ist2,ist,istr,ileg) = orig + perturbation
              call lintran_calculate(execution,atm,set,lintran_instance,rint_pert,stat)

              drv_fd%drint_bdrf_0(ist2,ist,istr,ileg,:,:) = (rint_pert - rint) / perturbation
              drv_fdsmall%drint_bdrf_0(ist2,ist,istr,ileg,:,:) = 10.0*(rint_pertsmall - rint) / perturbation
              atm%bdrf_0(ist2,ist,istr,ileg) = orig

            enddo
          enddo
        enddo
      enddo

      ! Bi-directional reflection function with viewing direction.
      write(6,"(A)") "Bi-directional reflection function (instrument)"
      do ileg = 0,nleg
        write(6,"(A,I4)") "Fourier number ",ileg
        do istr = 1,nstrhalf
          do ist = 1,nst
            do ist2 = 1,nst

              orig_geo = atm%bdrf_v(ist2,ist,istr,ileg,:)

              atm%bdrf_v(ist2,ist,istr,ileg,:) = orig_geo + 0.1 * perturbation
              call lintran_calculate(execution,atm,set,lintran_instance,rint_pertsmall,stat)

              atm%bdrf_v(ist2,ist,istr,ileg,:) = orig_geo + perturbation
              call lintran_calculate(execution,atm,set,lintran_instance,rint_pert,stat)

              drv_fd%drint_bdrf_v(ist2,ist,istr,ileg,:,:) = (rint_pert - rint) / perturbation
              drv_fdsmall%drint_bdrf_v(ist2,ist,istr,ileg,:,:) = 10.0*(rint_pertsmall - rint) / perturbation

              atm%bdrf_v(ist2,ist,istr,ileg,:) = orig_geo

            enddo
          enddo
        enddo
      enddo

      ! Bi-directional reflection function at single-scattering geometry.
      write(6,"(A)") "Bi-directional reflection function (single-scattering geometry)"
      do ist = 1,nst
        do ist2 = 1,nst

          orig_geo = atm%bdrf_ssg(ist2,ist,:)

          atm%bdrf_ssg(ist2,ist,:) = orig_geo + 0.1 * perturbation
          call lintran_calculate(execution,atm,set,lintran_instance,rint_pertsmall,stat)

          atm%bdrf_ssg(ist2,ist,:) = orig_geo + perturbation
          call lintran_calculate(execution,atm,set,lintran_instance,rint_pert,stat)

          drv_fd%drint_bdrf_ssg(ist2,ist,:,:) = (rint_pert - rint) / perturbation
          drv_fdsmall%drint_bdrf_ssg(ist2,ist,:,:) = 10.0*(rint_pertsmall - rint) / perturbation

          atm%bdrf_ssg(ist2,ist,:) = orig_geo

        enddo
      enddo

      ! Emissivity to internal stream.
      write(6,"(A)") "Surface emissivity"
      do ileg = 0,nleg
        do istr = 1,nstrhalf
          do ist = 1,nst

            orig = atm%emi(ist,istr,ileg)

            atm%emi(ist,istr,ileg) = orig + 0.1 * perturbation
            call lintran_calculate(execution,atm,set,lintran_instance,rint_pertsmall,stat)

            atm%emi(ist,istr,ileg) = orig + perturbation
            call lintran_calculate(execution,atm,set,lintran_instance,rint_pert,stat)

            drv_fd%drint_emi(ist,istr,ileg,:,:) = (rint_pert - rint) / perturbation
            drv_fdsmall%drint_emi(ist,istr,ileg,:,:) = 10.0*(rint_pertsmall - rint) / perturbation
            atm%emi(ist,istr,ileg) = orig

          enddo
        enddo
      enddo

      ! Emissivity at single-scattering geometry.
      write(6,"(A)") "Surface emissivity (single-scattering geometry)"
      do ist = 1,nst

        orig_geo = atm%emi_ssg(ist,:)

        atm%emi_ssg(ist,:) = orig_geo + 0.1 * perturbation
        call lintran_calculate(execution,atm,set,lintran_instance,rint_pertsmall,stat)

        atm%emi_ssg(ist,:) = orig_geo + perturbation
        call lintran_calculate(execution,atm,set,lintran_instance,rint_pert,stat)

        drv_fd%drint_emi_ssg(ist,:,:) = (rint_pert - rint) / perturbation
        drv_fdsmall%drint_emi_ssg(ist,:,:) = 10.0*(rint_pertsmall - rint) / perturbation

        atm%emi_ssg(ist,:) = orig_geo

      enddo

      ! Planck curve.
      write(6,"(A)") "Planck curve"
      do ideriv = 1,nlay_deriv_base

        ilay = set%ilay_deriv_base(ideriv)
        orig = atm%planck_curve(ilay)

        atm%planck_curve(ilay) = orig + 0.1 * perturbation
        call lintran_calculate(execution,atm,set,lintran_instance,rint_pertsmall,stat)
        atm%planck_curve(ilay) = orig + perturbation
        call lintran_calculate(execution,atm,set,lintran_instance,rint_pert,stat)

        drv_fd%drint_planck_curve(ideriv,:,:) = (rint_pert - rint) / perturbation
        drv_fdsmall%drint_planck_curve(ideriv,:,:) = 10.0*(rint_pertsmall - rint) / perturbation

        atm%planck_curve(ilay) = orig

      enddo

      deallocate(orig_geo)
      deallocate(rint_pert)
      deallocate(rint_pertsmall)

      ! }}}

    else

      nfile = 1

    endif

    ! Write run or runs. {{{
    do ifile = 1,nfile

      if (ifile .eq. 1) then
        asciiunit = asciiunit_result
        drv_write => drv
      endif
      if (ifile .eq. 2) then
        asciiunit = asciiunit_result_fd
        drv_write => drv_fd
      endif
      if (ifile .eq. 3) then
        asciiunit = asciiunit_result_fdsmall
        drv_write => drv_fdsmall
      endif

      do igeo = 1,ngeo

        write(asciiunit,"(A)") 'rint'
        write(asciiunit,'(1P,E21.12)') rint(:,igeo)
        write(asciiunit,"(A)") 'drint_taus'
        do ideriv = 1,nlay_deriv_base
          write(asciiunit,"(A,I4)") "Layer ",set%ilay_deriv_base(ideriv)
          write(asciiunit,'(1P,E21.12)') drv_write%drint_taus(ideriv,:,igeo)
        enddo
        write(asciiunit,"(A)") 'drint_taua'
        do ideriv = 1,nlay_deriv_base
          write(asciiunit,"(A,I4)") "Layer ",set%ilay_deriv_base(ideriv)
          write(asciiunit,'(1P,E21.12)') drv_write%drint_taua(ideriv,:,igeo)
        enddo
        write(asciiunit,"(A)") 'drint_coefs'
        do ideriv = 1,nlay_deriv_ph
          write(asciiunit,"(A,I4)") "Layer ",set%ilay_deriv_ph(ideriv)
          do ileg = 0,nleg_deriv
            do iscat = 1,nscat
              write(asciiunit,'(1P,E21.12)') drv_write%drint_coefs(iscat,ileg,ideriv,:,igeo)
            enddo
          enddo
        enddo
        write(asciiunit,"(A)") 'drint_phase_ssg'
        do ideriv = 1,nlay_deriv_ph
          write(asciiunit,"(A,I4)") "Layer ",set%ilay_deriv_ph(ideriv)
          do iscat = 1,nscat
            write(asciiunit,'(1P,E21.12)') drv_write%drint_phase_ssg(iscat,ideriv,:,igeo)
          enddo
        enddo
        write(asciiunit,"(A)") 'drint_bdrf'
        do ileg = 0,nleg
          do istr = 1,nstrhalf
            do istr2 = 1,nstrhalf
              do ist = 1,nst
                do ist2 = 1,nst
                  write(asciiunit,'(1P,E21.12)') drv_write%drint_bdrf(ist2,ist,istr2,istr,ileg,:,igeo)
                enddo
              enddo
            enddo
          enddo
        enddo
        write(asciiunit,"(A)") 'drint_bdrf_0'
        do ileg = 0,nleg
          do istr = 1,nstrhalf
            do ist = 1,nst
              do ist2 = 1,nst
                write(asciiunit,'(1P,E21.12)') drv_write%drint_bdrf_0(ist2,ist,istr,ileg,:,igeo)
              enddo
            enddo
          enddo
        enddo
        write(asciiunit,"(A)") 'drint_bdrf_v'
        do ileg = 0,nleg
          do istr = 1,nstrhalf
            do ist = 1,nst
              do ist2 = 1,nst
                write(asciiunit,'(1P,E21.12)') drv_write%drint_bdrf_v(ist2,ist,istr,ileg,:,igeo)
              enddo
            enddo
          enddo
        enddo
        write(asciiunit,"(A)") 'drint_bdrf_ssg'
        do ist = 1,nst
          do ist2 = 1,nst
            write(asciiunit,'(1P,E21.12)') drv_write%drint_bdrf_ssg(ist2,ist,:,igeo)
          enddo
        enddo
        write(asciiunit,"(A)") 'drint_emi'
        do ileg = 0,nleg
          do istr = 1,nstrhalf
            do ist = 1,nst
              write(asciiunit,'(1P,E21.12)') drv_write%drint_emi(ist,istr,ileg,:,igeo)
            enddo
          enddo
        enddo
        write(asciiunit,"(A)") 'drint_emi_ssg'
        do ist = 1,nst
          write(asciiunit,'(1P,E21.12)') drv_write%drint_emi_ssg(ist,:,igeo)
        enddo
        write(asciiunit,"(A)") 'drint_planck_curve'
        do ideriv = 1,nlay_deriv_base
          write(asciiunit,"(A,I4)") "Layer ",set%ilay_deriv_base(ideriv)
          write(asciiunit,'(1P,E21.12)') drv_write%drint_planck_curve(ideriv,:,igeo)
        enddo

      enddo

      close(asciiunit)

    enddo

    ! Write calculation time on screen.
    write(6,"(A,I4.4,A,1P,E21.12)") "CPU time single run with derivatives (",runid,"): ",time_fin-time_ini

    ! }}}

  else

    ! Without derivatives it is easy. There is no finite-difference and only one
    ! field of results.
    do igeo = 1,ngeo

      write(asciiunit_result,"(A)") 'rint'
      write(asciiunit_result,'(1P,E21.12)') rint(:,igeo)

    enddo

    close(asciiunit_result)

    ! Write calculation time on screen.
    write(6,"(A,I4.4,A,1P,E21.12)") "CPU time single run without derivatives (",runid,"): ",time_fin-time_ini

  endif

  ! Clean arrays that are not members of Lintran structures.
  deallocate(zlev)
  deallocate(mu_v)
  deallocate(azi)
  deallocate(rint)

  ! Close off Lintran.
  call lintran_close(lintran_instance,stat)

  ! Clean members of the interface of Lintran.
  if (allocated(atm%taus)) deallocate(atm%taus)
  if (allocated(atm%taua)) deallocate(atm%taua)
  if (allocated(atm%coefs)) deallocate(atm%coefs)
  if (allocated(atm%nleg_lay)) deallocate(atm%nleg_lay)
  if (allocated(atm%phase_ssg)) deallocate(atm%phase_ssg)
  if (allocated(atm%bdrf_ssg)) deallocate(atm%bdrf_ssg)
  if (allocated(atm%bdrf_0)) deallocate(atm%bdrf_0)
  if (allocated(atm%bdrf_v)) deallocate(atm%bdrf_v)
  if (allocated(atm%bdrf)) deallocate(atm%bdrf)
  if (allocated(atm%emi)) deallocate(atm%emi)
  if (allocated(atm%emi_ssg)) deallocate(atm%emi_ssg)
  if (allocated(atm%planck_curve)) deallocate(atm%planck_curve)
  deallocate(set%execute_stokes)
  if (execute_differentiation) then
    deallocate(set%differentiate_stokes)
    deallocate(set%ilay_deriv_base)
    deallocate(set%ilay_deriv_ph)
    deallocate(drv%drint_taus)
    deallocate(drv%drint_taua)
    deallocate(drv%drint_coefs)
    deallocate(drv%drint_phase_ssg)
    deallocate(drv%drint_bdrf_ssg)
    deallocate(drv%drint_bdrf_0)
    deallocate(drv%drint_bdrf_v)
    deallocate(drv%drint_bdrf)
    deallocate(drv%drint_emi)
    deallocate(drv%drint_emi_ssg)
    deallocate(drv%drint_planck_curve)
    if (finite_difference) then
      deallocate(drv_fd%drint_taus)
      deallocate(drv_fd%drint_taua)
      deallocate(drv_fd%drint_coefs)
      deallocate(drv_fd%drint_phase_ssg)
      deallocate(drv_fd%drint_bdrf_ssg)
      deallocate(drv_fd%drint_bdrf_0)
      deallocate(drv_fd%drint_bdrf_v)
      deallocate(drv_fd%drint_bdrf)
      deallocate(drv_fd%drint_emi)
      deallocate(drv_fd%drint_emi_ssg)
      deallocate(drv_fd%drint_planck_curve)
      deallocate(drv_fdsmall%drint_taus)
      deallocate(drv_fdsmall%drint_taua)
      deallocate(drv_fdsmall%drint_coefs)
      deallocate(drv_fdsmall%drint_phase_ssg)
      deallocate(drv_fdsmall%drint_bdrf_ssg)
      deallocate(drv_fdsmall%drint_bdrf_0)
      deallocate(drv_fdsmall%drint_bdrf_v)
      deallocate(drv_fdsmall%drint_bdrf)
      deallocate(drv_fdsmall%drint_emi)
      deallocate(drv_fdsmall%drint_emi_ssg)
      deallocate(drv_fdsmall%drint_planck_curve)
    endif
  endif

contains

  function search_interval(xval,xarray) ! {{{

    ! Gets the two indices in xarray, between which xval is.
    ! The array xarray needs to be strictly ascending, but need not
    ! be equidistant.

    implicit none

    integer, dimension(2) :: search_interval ! Result
    real, dimension(:), intent(in) :: xarray ! Array in which the interval is searched
    real, intent(in) :: xval ! Value that is searched
    integer :: sz ! Size of xarray

    search_interval(2:2) = minloc(xarray,mask=xarray .gt. xval)
    if (search_interval(2) .eq. 0) then
      sz = size(xarray)
      search_interval = (/sz-1,sz/)
    else if (search_interval(2) .eq. 1) then
      search_interval = (/1,2/)
    else
      search_interval(1) = search_interval(2) - 1
    endif

  end function search_interval ! }}}

  subroutine atmosphere_write(atm,filename) ! {{{

    use netcdf, only: nf90_create, nf90_netcdf4, nf90_def_dim, nf90_def_var, nf90_int, nf90_double, nf90_enddef, nf90_put_var, nf90_close
    use lintran_types_module, only: lintran_atmosphere

    implicit none

    ! Input and output.
    type(lintran_atmosphere), intent(in) :: atm !< The atmosphere to be written into the NetCDF.
    character(len=*), intent(in) :: filename !< NetCDF file name.

    ! NetCDF identifiers.
    integer :: ncid
    integer, dimension(32) :: dimid
    integer, dimension(16) :: varid

    ! Empty array for dimensions for a scalar, such as the sun.
    integer, dimension(0), parameter :: emptyness = 0

    ! Everyone has its own dimensions, because the atmosphere needs not be to properly
    ! dimensioned. 

    ! Dimension sizes that are defined this way. {{{
    integer :: taus_nlay
    integer :: taua_nlay
    integer :: coefs_nscat
    integer :: coefs_nleg
    integer :: coefs_nlay
    integer :: phase_ssg_nscat
    integer :: phase_ssg_nlay
    integer :: phase_ssg_ngeo
    integer :: bdrf_nst1
    integer :: bdrf_nst2
    integer :: bdrf_nstrhalf1
    integer :: bdrf_nstrhalf2
    integer :: bdrf_nleg
    integer :: bdrf_0_nst1
    integer :: bdrf_0_nst2
    integer :: bdrf_0_nstrhalf
    integer :: bdrf_0_nleg
    integer :: bdrf_v_nst1
    integer :: bdrf_v_nst2
    integer :: bdrf_v_nstrhalf
    integer :: bdrf_v_nleg
    integer :: bdrf_v_ngeo
    integer :: bdrf_ssg_nst1
    integer :: bdrf_ssg_nst2
    integer :: bdrf_ssg_ngeo
    integer :: emi_nst
    integer :: emi_nstrhalf
    integer :: emi_nleg
    integer :: emi_ssg_nst
    integer :: emi_ssg_ngeo
    integer :: planck_curve_nlay
    integer :: nleg_lay_nlay
    ! }}}

    ! Create the NetCDF file.
    call check(nf90_create(trim(filename),nf90_netcdf4,ncid))

    ! For each variable, create its dimensions and the variable. No dimensions are
    ! shared, so the atmosphere is just copied independent of whether or not it is
    ! properly defined. Non-allocated arrays are not written. It is possible that
    ! some arrays need not be allocated, but this routine can also write illegal
    ! atmospheres. For scalars, they are always allocated, so they are always written.
    ! We cannot smell whether or not the scalars where initialized or not.
    call check(nf90_def_var(ncid,"sun",nf90_double,emptyness,varid(1)))
    if (allocated(atm%taus)) then
      taus_nlay = size(atm%taus,1)
      call check(nf90_def_dim(ncid,"taus_nlay",taus_nlay,dimid(1)))
      call check(nf90_def_var(ncid,"taus",nf90_double,dimid(1:1),varid(2)))
    endif
    if (allocated(atm%taua)) then
      taua_nlay = size(atm%taua,1)
      call check(nf90_def_dim(ncid,"taua_nlay",taua_nlay,dimid(2)))
      call check(nf90_def_var(ncid,"taua",nf90_double,dimid(2:2),varid(3)))
    endif
    if (allocated(atm%coefs)) then
      coefs_nscat = size(atm%coefs,1)
      coefs_nleg = size(atm%coefs,2)
      coefs_nlay = size(atm%coefs,3)
      call check(nf90_def_dim(ncid,"coefs_nscat",coefs_nscat,dimid(3)))
      call check(nf90_def_dim(ncid,"coefs_nleg",coefs_nleg,dimid(4)))
      call check(nf90_def_dim(ncid,"coefs_nlay",coefs_nlay,dimid(5)))
      call check(nf90_def_var(ncid,"coefs",nf90_double,dimid(3:5),varid(4)))
    endif
    if (allocated(atm%phase_ssg)) then
      phase_ssg_nscat = size(atm%phase_ssg,1)
      phase_ssg_nlay = size(atm%phase_ssg,2)
      phase_ssg_ngeo = size(atm%phase_ssg,3)
      call check(nf90_def_dim(ncid,"phase_ssg_nscat",phase_ssg_nscat,dimid(6)))
      call check(nf90_def_dim(ncid,"phase_ssg_nlay",phase_ssg_nlay,dimid(7)))
      call check(nf90_def_dim(ncid,"phase_ssg_ngeo",phase_ssg_ngeo,dimid(8)))
      call check(nf90_def_var(ncid,"phase_ssg",nf90_double,dimid(6:8),varid(5)))
    endif
    if (allocated(atm%bdrf)) then
      bdrf_nst1 = size(atm%bdrf,1)
      bdrf_nst2 = size(atm%bdrf,2)
      bdrf_nstrhalf1 = size(atm%bdrf,3)
      bdrf_nstrhalf2 = size(atm%bdrf,4)
      bdrf_nleg = size(atm%bdrf,5)
      call check(nf90_def_dim(ncid,"bdrf_nst1",bdrf_nst1,dimid(9)))
      call check(nf90_def_dim(ncid,"bdrf_nst2",bdrf_nst2,dimid(10)))
      call check(nf90_def_dim(ncid,"bdrf_nstrhalf1",bdrf_nstrhalf1,dimid(11)))
      call check(nf90_def_dim(ncid,"bdrf_nstrhalf2",bdrf_nstrhalf2,dimid(12)))
      call check(nf90_def_dim(ncid,"bdrf_nleg",bdrf_nleg,dimid(13)))
      call check(nf90_def_var(ncid,"bdrf",nf90_double,dimid(9:13),varid(6)))
    endif
    if (allocated(atm%bdrf_0)) then
      bdrf_0_nst1 = size(atm%bdrf_0,1)
      bdrf_0_nst2 = size(atm%bdrf_0,2)
      bdrf_0_nstrhalf = size(atm%bdrf_0,3)
      bdrf_0_nleg = size(atm%bdrf_0,4)
      call check(nf90_def_dim(ncid,"bdrf_0_nst1",bdrf_0_nst1,dimid(14)))
      call check(nf90_def_dim(ncid,"bdrf_0_nst2",bdrf_0_nst2,dimid(15)))
      call check(nf90_def_dim(ncid,"bdrf_0_nstrhalf",bdrf_0_nstrhalf,dimid(16)))
      call check(nf90_def_dim(ncid,"bdrf_0_nleg",bdrf_0_nleg,dimid(17)))
      call check(nf90_def_var(ncid,"bdrf_0",nf90_double,dimid(14:17),varid(7)))
    endif
    if (allocated(atm%bdrf_v)) then
      bdrf_v_nst1 = size(atm%bdrf_v,1)
      bdrf_v_nst2 = size(atm%bdrf_v,2)
      bdrf_v_nstrhalf = size(atm%bdrf_v,3)
      bdrf_v_nleg = size(atm%bdrf_v,4)
      bdrf_v_ngeo = size(atm%bdrf_v,5)
      call check(nf90_def_dim(ncid,"bdrf_v_nst1",bdrf_v_nst1,dimid(18)))
      call check(nf90_def_dim(ncid,"bdrf_v_nst2",bdrf_v_nst2,dimid(19)))
      call check(nf90_def_dim(ncid,"bdrf_v_nstrhalf",bdrf_v_nstrhalf,dimid(20)))
      call check(nf90_def_dim(ncid,"bdrf_v_nleg",bdrf_v_nleg,dimid(21)))
      call check(nf90_def_dim(ncid,"bdrf_v_ngeo",bdrf_v_ngeo,dimid(22)))
      call check(nf90_def_var(ncid,"bdrf_v",nf90_double,dimid(18:22),varid(8)))
    endif
    if (allocated(atm%bdrf_ssg)) then
      bdrf_ssg_nst1 = size(atm%bdrf_ssg,1)
      bdrf_ssg_nst2 = size(atm%bdrf_ssg,2)
      bdrf_ssg_ngeo = size(atm%bdrf_ssg,3)
      call check(nf90_def_dim(ncid,"bdrf_ssg_nst1",bdrf_ssg_nst1,dimid(23)))
      call check(nf90_def_dim(ncid,"bdrf_ssg_nst2",bdrf_ssg_nst2,dimid(24)))
      call check(nf90_def_dim(ncid,"bdrf_ssg_ngeo",bdrf_ssg_ngeo,dimid(25)))
      call check(nf90_def_var(ncid,"bdrf_ssg",nf90_double,dimid(23:25),varid(9)))
    endif
    if (allocated(atm%emi)) then
      emi_nst = size(atm%emi,1)
      emi_nstrhalf = size(atm%emi,2)
      emi_nleg = size(atm%emi,3)
      call check(nf90_def_dim(ncid,"emi_nst",emi_nst,dimid(26)))
      call check(nf90_def_dim(ncid,"emi_nstrhalf",emi_nstrhalf,dimid(27)))
      call check(nf90_def_dim(ncid,"emi_nleg",emi_nleg,dimid(28)))
      call check(nf90_def_var(ncid,"emi",nf90_double,dimid(26:28),varid(10)))
    endif
    if (allocated(atm%emi_ssg)) then
      emi_ssg_nst = size(atm%emi_ssg,1)
      emi_ssg_ngeo = size(atm%emi_ssg,2)
      call check(nf90_def_dim(ncid,"emi_ssg_nst",emi_ssg_nst,dimid(29)))
      call check(nf90_def_dim(ncid,"emi_ssg_ngeo",emi_ssg_ngeo,dimid(30)))
      call check(nf90_def_var(ncid,"emi_ssg",nf90_double,dimid(29:30),varid(11)))
    endif
    if (allocated(atm%planck_curve)) then
      planck_curve_nlay = size(atm%planck_curve,1)
      call check(nf90_def_dim(ncid,"planck_curve_nlay",planck_curve_nlay,dimid(31)))
      call check(nf90_def_var(ncid,"planck_curve",nf90_double,dimid(31:31),varid(12)))
    endif
    if (allocated(atm%nleg_lay)) then
      nleg_lay_nlay = size(atm%nleg_lay,1)
      call check(nf90_def_dim(ncid,"nleg_lay_nlay",nleg_lay_nlay,dimid(32)))
      call check(nf90_def_var(ncid,"nleg_lay",nf90_int,dimid(32:32),varid(13)))
    endif
    ! These booleans are written as an integer, a 0 for false and a 1 for true.
    call check(nf90_def_var(ncid,"bdrf_only_0",nf90_int,emptyness,varid(14)))
    call check(nf90_def_var(ncid,"emi_only_0",nf90_int,emptyness,varid(15)))
    call check(nf90_def_var(ncid,"thermal_emission",nf90_int,emptyness,varid(16)))

    ! Now, we are ready with defining the dimensions and the variables.
    call check(nf90_enddef(ncid))

    ! Re-open all the if-allocated clauses. Otherwise, we had to enddef and redef all
    ! the time.
    call check(nf90_put_var(ncid,varid(1),atm%sun))
    if (allocated(atm%taus)) call check(nf90_put_var(ncid,varid(2),atm%taus))
    if (allocated(atm%taua)) call check(nf90_put_var(ncid,varid(3),atm%taua))
    if (allocated(atm%coefs)) call check(nf90_put_var(ncid,varid(4),atm%coefs))
    if (allocated(atm%phase_ssg)) call check(nf90_put_var(ncid,varid(5),atm%phase_ssg))
    if (allocated(atm%bdrf)) call check(nf90_put_var(ncid,varid(6),atm%bdrf))
    if (allocated(atm%bdrf_0)) call check(nf90_put_var(ncid,varid(7),atm%bdrf_0))
    if (allocated(atm%bdrf_v)) call check(nf90_put_var(ncid,varid(8),atm%bdrf_v))
    if (allocated(atm%bdrf_ssg)) call check(nf90_put_var(ncid,varid(9),atm%bdrf_ssg))
    if (allocated(atm%emi)) call check(nf90_put_var(ncid,varid(10),atm%emi))
    if (allocated(atm%emi_ssg)) call check(nf90_put_var(ncid,varid(11),atm%emi_ssg))
    if (allocated(atm%planck_curve)) call check(nf90_put_var(ncid,varid(12),atm%planck_curve))
    if (allocated(atm%nleg_lay)) call check(nf90_put_var(ncid,varid(13),atm%nleg_lay))
    ! Now, the booleans. They are all scalar.
    if (atm%bdrf_only_0) then
      call check(nf90_put_var(ncid,varid(14),1))
    else
      call check(nf90_put_var(ncid,varid(14),0))
    endif
    if (atm%emi_only_0) then
      call check(nf90_put_var(ncid,varid(15),1))
    else
      call check(nf90_put_var(ncid,varid(15),0))
    endif
    if (atm%thermal_emission) then
      call check(nf90_put_var(ncid,varid(16),1))
    else
      call check(nf90_put_var(ncid,varid(16),0))
    endif

    ! Ready.
    call check(nf90_close(ncid))

  end subroutine atmosphere_write ! }}}

  subroutine atmosphere_read(filename,atm) ! {{{

    use netcdf, only: nf90_open, nf90_nowrite, nf90_inq_dimid, nf90_inquire_dimension, nf90_inq_varid, nf90_noerr, nf90_get_var, nf90_close
    use lintran_types_module, only: lintran_atmosphere

    implicit none

    ! Input and output.
    character(len=*), intent(in) :: filename !< NetCDF file name.
    type(lintran_atmosphere), intent(out) :: atm !< The atmosphere to be read from the NetCDF.

    ! NetCDF identifiers.
    integer :: ncid
    integer :: dimid
    integer :: varid

    ! NetCDF error that is allowed and used as signal that a variable does not exist.
    integer :: stat

    ! Boolean read. A 1 is true and a 0 is false.
    integer :: boolread

    ! Everyone has its own dimensions, because the atmosphere needs not be to properly
    ! dimensioned. 

    ! Dimension sizes that are defined this way. {{{
    integer :: taus_nlay
    integer :: taua_nlay
    integer :: coefs_nscat
    integer :: coefs_nleg
    integer :: coefs_nlay
    integer :: phase_ssg_nscat
    integer :: phase_ssg_nlay
    integer :: phase_ssg_ngeo
    integer :: bdrf_nst1
    integer :: bdrf_nst2
    integer :: bdrf_nstrhalf1
    integer :: bdrf_nstrhalf2
    integer :: bdrf_nleg
    integer :: bdrf_0_nst1
    integer :: bdrf_0_nst2
    integer :: bdrf_0_nstrhalf
    integer :: bdrf_0_nleg
    integer :: bdrf_v_nst1
    integer :: bdrf_v_nst2
    integer :: bdrf_v_nstrhalf
    integer :: bdrf_v_nleg
    integer :: bdrf_v_ngeo
    integer :: bdrf_ssg_nst1
    integer :: bdrf_ssg_nst2
    integer :: bdrf_ssg_ngeo
    integer :: emi_nst
    integer :: emi_nstrhalf
    integer :: emi_nleg
    integer :: emi_ssg_nst
    integer :: emi_ssg_ngeo
    integer :: planck_curve_nlay
    integer :: nleg_lay_nlay
    ! }}}

    ! Open the NetCDF file.
    call check(nf90_open(trim(filename),nf90_nowrite,ncid))

    ! For each variable, check if it exists. If it exists, read the dimensions and
    ! the contents. Scalar variables must exist, because they were also always written
    ! in the write routine. Then, the entire contents of the NetCDF has been turned into
    ! the stucture regardless of whether or not it makes sense.
    call check(nf90_inq_varid(ncid,"sun",varid))
    call check(nf90_get_var(ncid,varid,atm%sun))
    stat = nf90_inq_varid(ncid,"taus",varid)
    if (stat .eq. nf90_noerr) then
      call check(nf90_inq_dimid(ncid,"taus_nlay",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=taus_nlay))
      allocate(atm%taus(taus_nlay))
      call check(nf90_get_var(ncid,varid,atm%taus))
    endif
    stat = nf90_inq_varid(ncid,"taua",varid)
    if (stat .eq. nf90_noerr) then
      call check(nf90_inq_dimid(ncid,"taua_nlay",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=taua_nlay))
      allocate(atm%taua(taua_nlay))
      call check(nf90_get_var(ncid,varid,atm%taua))
    endif
    stat = nf90_inq_varid(ncid,"coefs",varid)
    if (stat .eq. nf90_noerr) then
      call check(nf90_inq_dimid(ncid,"coefs_nscat",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=coefs_nscat))
      call check(nf90_inq_dimid(ncid,"coefs_nleg",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=coefs_nleg))
      call check(nf90_inq_dimid(ncid,"coefs_nlay",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=coefs_nlay))
      allocate(atm%coefs(coefs_nscat,0:coefs_nleg-1,coefs_nlay))
      call check(nf90_get_var(ncid,varid,atm%coefs))
    endif
    stat = nf90_inq_varid(ncid,"phase_ssg",varid)
    if (stat .eq. nf90_noerr) then
      call check(nf90_inq_dimid(ncid,"phase_ssg_nscat",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=phase_ssg_nscat))
      call check(nf90_inq_dimid(ncid,"phase_ssg_nlay",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=phase_ssg_nlay))
      call check(nf90_inq_dimid(ncid,"phase_ssg_ngeo",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=phase_ssg_ngeo))
      allocate(atm%phase_ssg(phase_ssg_nscat,phase_ssg_nlay,phase_ssg_ngeo))
      call check(nf90_get_var(ncid,varid,atm%phase_ssg))
    endif
    stat = nf90_inq_varid(ncid,"bdrf",varid)
    if (stat .eq. nf90_noerr) then
      call check(nf90_inq_dimid(ncid,"bdrf_nst1",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=bdrf_nst1))
      call check(nf90_inq_dimid(ncid,"bdrf_nst2",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=bdrf_nst2))
      call check(nf90_inq_dimid(ncid,"bdrf_nstrhalf1",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=bdrf_nstrhalf1))
      call check(nf90_inq_dimid(ncid,"bdrf_nstrhalf2",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=bdrf_nstrhalf2))
      call check(nf90_inq_dimid(ncid,"bdrf_nleg",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=bdrf_nleg))
      allocate(atm%bdrf(bdrf_nst1,bdrf_nst2,bdrf_nstrhalf1,bdrf_nstrhalf2,0:bdrf_nleg-1))
      call check(nf90_get_var(ncid,varid,atm%bdrf))
    endif
    stat = nf90_inq_varid(ncid,"bdrf_0",varid)
    if (stat .eq. nf90_noerr) then
      call check(nf90_inq_dimid(ncid,"bdrf_0_nst1",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=bdrf_0_nst1))
      call check(nf90_inq_dimid(ncid,"bdrf_0_nst2",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=bdrf_0_nst2))
      call check(nf90_inq_dimid(ncid,"bdrf_0_nstrhalf",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=bdrf_0_nstrhalf))
      call check(nf90_inq_dimid(ncid,"bdrf_0_nleg",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=bdrf_0_nleg))
      allocate(atm%bdrf_0(bdrf_0_nst1,bdrf_0_nst2,bdrf_0_nstrhalf,0:bdrf_0_nleg-1))
      call check(nf90_get_var(ncid,varid,atm%bdrf_0))
    endif
    stat = nf90_inq_varid(ncid,"bdrf_v",varid)
    if (stat .eq. nf90_noerr) then
      call check(nf90_inq_dimid(ncid,"bdrf_v_nst1",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=bdrf_v_nst1))
      call check(nf90_inq_dimid(ncid,"bdrf_v_nst2",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=bdrf_v_nst2))
      call check(nf90_inq_dimid(ncid,"bdrf_v_nstrhalf",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=bdrf_v_nstrhalf))
      call check(nf90_inq_dimid(ncid,"bdrf_v_nleg",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=bdrf_v_nleg))
      call check(nf90_inq_dimid(ncid,"bdrf_v_ngeo",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=bdrf_v_ngeo))
      allocate(atm%bdrf_v(bdrf_v_nst1,bdrf_v_nst2,bdrf_v_nstrhalf,0:bdrf_v_nleg-1,bdrf_v_ngeo))
      call check(nf90_get_var(ncid,varid,atm%bdrf_v))
    endif
    stat = nf90_inq_varid(ncid,"bdrf_ssg",varid)
    if (stat .eq. nf90_noerr) then
      call check(nf90_inq_dimid(ncid,"bdrf_ssg_nst1",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=bdrf_ssg_nst1))
      call check(nf90_inq_dimid(ncid,"bdrf_ssg_nst2",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=bdrf_ssg_nst2))
      call check(nf90_inq_dimid(ncid,"bdrf_ssg_ngeo",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=bdrf_ssg_ngeo))
      allocate(atm%bdrf_ssg(bdrf_ssg_nst1,bdrf_ssg_nst2,bdrf_ssg_ngeo))
      call check(nf90_get_var(ncid,varid,atm%bdrf_ssg))
    endif
    stat = nf90_inq_varid(ncid,"emi",varid)
    if (stat .eq. nf90_noerr) then
      call check(nf90_inq_dimid(ncid,"emi_nst",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=emi_nst))
      call check(nf90_inq_dimid(ncid,"emi_nstrhalf",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=emi_nstrhalf))
      call check(nf90_inq_dimid(ncid,"emi_nleg",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=emi_nleg))
      allocate(atm%emi(emi_nst,emi_nstrhalf,0:emi_nleg-1))
      call check(nf90_get_var(ncid,varid,atm%emi))
    endif
    stat = nf90_inq_varid(ncid,"emi_ssg",varid)
    if (stat .eq. nf90_noerr) then
      call check(nf90_inq_dimid(ncid,"emi_ssg_nst",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=emi_ssg_nst))
      call check(nf90_inq_dimid(ncid,"emi_ssg_ngeo",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=emi_ssg_ngeo))
      allocate(atm%emi_ssg(emi_ssg_nst,emi_ssg_ngeo))
      call check(nf90_get_var(ncid,varid,atm%emi_ssg))
    endif
    stat = nf90_inq_varid(ncid,"planck_curve",varid)
    if (stat .eq. nf90_noerr) then
      call check(nf90_inq_dimid(ncid,"planck_curve_nlay",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=planck_curve_nlay))
      allocate(atm%planck_curve(planck_curve_nlay))
      call check(nf90_get_var(ncid,varid,atm%planck_curve))
    endif
    stat = nf90_inq_varid(ncid,"nleg_lay",varid)
    if (stat .eq. nf90_noerr) then
      call check(nf90_inq_dimid(ncid,"nleg_lay_nlay",dimid))
      call check(nf90_inquire_dimension(ncid,dimid,len=nleg_lay_nlay))
      allocate(atm%nleg_lay(nleg_lay_nlay))
      call check(nf90_get_var(ncid,varid,atm%nleg_lay))
    endif
    call check(nf90_inq_varid(ncid,"bdrf_only_0",varid))
    call check(nf90_get_var(ncid,varid,boolread))
    atm%bdrf_only_0 = boolread .eq. 1
    call check(nf90_inq_varid(ncid,"emi_only_0",varid))
    call check(nf90_get_var(ncid,varid,boolread))
    atm%emi_only_0 = boolread .eq. 1
    call check(nf90_inq_varid(ncid,"thermal_emission",varid))
    call check(nf90_get_var(ncid,varid,boolread))
    atm%thermal_emission = boolread .eq. 1

    ! Ready.
    call check(nf90_close(ncid))

  end subroutine atmosphere_read ! }}}

  subroutine atmosphere_derive_phase_ssg(ngeo,mu_0,mu_v,azi,atm) ! {{{

    use lintran_constants_module, only: iscat_a1, iscat_a2, iscat_a3, iscat_b1, iscat_a4, iscat_b2
    use lintran_types_module, only: lintran_atmosphere

    implicit none

    ! Input and output.
    integer, intent(in) :: ngeo !< Number of viewing geometries.
    real, intent(in) :: mu_0 !< Cosine of solar zenith angle.
    real, dimension(ngeo), intent(in) :: mu_v !< Cosines of viewing angles.
    real, dimension(ngeo), intent(in) :: azi !< Relative azimuth angles.
    type(lintran_atmosphere), intent(inout) :: atm !< The atmosphere in which the single-scattering phase function is replaced.

    ! Generalized spherical functions.
    real, dimension(0:size(atm%coefs,2)-1) :: gsf_00
    real, dimension(0:size(atm%coefs,2)-1) :: gsf_02
    real, dimension(0:size(atm%coefs,2)-1) :: gsf_22
    real, dimension(0:size(atm%coefs,2)-1) :: gsf_2m2

    ! Dimension sizes.
    integer :: nscat ! Number of independent matrix elements.
    integer :: nleg ! Highest Legendre moment.
    integer :: nlay ! Number of atmospheric layers.
    integer :: nleg_lay_cur ! Highest Legendre moment to be taken into account for a layer.

    ! Iterators.
    integer :: ileg ! Over Legendre moments.
    integer :: igeo ! Over viewing geometries.

    ! Scattering angle at single-scattering geometry.
    real :: uscat_ssg

    ! Get the dimension sizes from the Legendre coefficients. That is
    ! where we derive everything from.
    nscat = size(atm%coefs,1)
    nleg = min(size(atm%coefs,2)-1,maxval(atm%nleg_lay))
    nlay = size(atm%coefs,3)

    ! We just take over the number of independent matrix elements
    ! and atmospheric layers from the coefficients. We hope it
    ! makes sense.

    ! Throw away eventual old single-scattering phase function.
    if (allocated(atm%phase_ssg)) deallocate(atm%phase_ssg)

    ! Make a new one with the best dimensions that we have.
    allocate(atm%phase_ssg(nscat,nlay,ngeo))

    do igeo = 1,ngeo

      ! Get the angle at single-scattering geometry. That determines
      ! the faith of the generalized spherical functions.
      uscat_ssg = -mu_0*mu_v(igeo) + sqrt((1.0-mu_0**2.0)*(1.0-mu_v(igeo)**2.0))*cos(azi(igeo))

      gsf_00(0) = 1.0
      ! Use recursive definition of associated Legendre polynomials to
      ! calculate their overlaps and thus the phase matrix derivatives
      do ileg = 1,nleg

        if (ileg .eq. 1) then
          ! Recursive construction, knowing that for l-2, the polynomials
          ! does not yet exist.
          gsf_00(ileg) = float(2*ileg-1)*uscat_ssg*gsf_00(ileg-1)/sqrt(float(ileg**2))
        else
          ! Full recursion.
          gsf_00(ileg) = (float(2*ileg-1)*uscat_ssg*gsf_00(ileg-1) - sqrt(float((ileg-1)**2))*gsf_00(ileg-2))/sqrt(float(ileg**2))
        endif

      enddo

      if (nscat .ge. 4) then ! Only for vector.

        gsf_02(0:1) = 0.0
        gsf_02(2) = -0.25 * sqrt(6.0) * (1.0 - uscat_ssg**2.0)
        gsf_22(0:1) = 0.0
        gsf_22(2) = 0.25 * (1.0 + uscat_ssg)**2.0
        gsf_2m2(0:1) = 0.0
        gsf_2m2(2) = 0.25 * (1.0 - uscat_ssg)**2.0

        ! Here, m=0.
        do ileg = 2,nleg-1

          gsf_02(ileg+1) = (1.0/(ileg*sqrt(((ileg+1.0)**2.0-4.0)*((ileg+1.0)**2.0)))) * ((2.0*ileg+1.0)*(ileg*(ileg+1.0)*uscat_ssg) * gsf_02(ileg) - (ileg+1.0)*sqrt((ileg**2.0-4.0)*(ileg**2.0)) * gsf_02(ileg-1))

        enddo

        ! Here m=2.
        do ileg = 2,nleg-1

          gsf_22(ileg+1) = (1.0/(ileg*sqrt(((ileg+1.0)**2.0-4.0)*((ileg+1.0)**2.0-4.0)))) * ((2.0*ileg+1.0)*(ileg*(ileg+1.0)*uscat_ssg-4.0) * gsf_22(ileg) - (ileg+1)*sqrt((ileg**2.0-4.0)*(ileg**2.0-4.0)) * gsf_22(ileg-1))
          gsf_2m2(ileg+1) = (1.0/(ileg*sqrt(((ileg+1.0)**2.0-4.0)*((ileg+1.0)**2.0-4.0)))) * ((2.0*ileg+1.0)*(ileg*(ileg+1.0)*uscat_ssg+4.0) * gsf_2m2(ileg) - (ileg+1)*sqrt((ileg**2.0-4.0)*(ileg**2.0-4.0)) * gsf_2m2(ileg-1))

        enddo

      endif

      do ilay = 1,nlay

        nleg_lay_cur = min(nleg,atm%nleg_lay(ilay))

        atm%phase_ssg(iscat_a1,ilay,igeo) = dot_product(gsf_00(0:nleg_lay_cur) , atm%coefs(iscat_a1,0:nleg_lay_cur,ilay))
        if (nscat .ge. 4) then
          atm%phase_ssg(iscat_a2,ilay,igeo) = 0.5 * (dot_product(gsf_22(0:nleg_lay_cur) , (atm%coefs(iscat_a2,0:nleg_lay_cur,ilay) + atm%coefs(iscat_a3,0:nleg_lay_cur,ilay))) + dot_product(gsf_2m2(0:nleg_lay_cur) , (atm%coefs(iscat_a2,0:nleg_lay_cur,ilay) - atm%coefs(iscat_a3,0:nleg_lay_cur,ilay))))
          atm%phase_ssg(iscat_a3,ilay,igeo) = 0.5 * (dot_product(gsf_22(0:nleg_lay_cur) , (atm%coefs(iscat_a2,0:nleg_lay_cur,ilay) + atm%coefs(iscat_a3,0:nleg_lay_cur,ilay))) - dot_product(gsf_2m2(0:nleg_lay_cur) , (atm%coefs(iscat_a2,0:nleg_lay_cur,ilay) - atm%coefs(iscat_a3,0:nleg_lay_cur,ilay))))
          atm%phase_ssg(iscat_b1,ilay,igeo) = dot_product(gsf_02(0:nleg_lay_cur) , atm%coefs(iscat_b1,0:nleg_lay_cur,ilay))
        endif
        if (nscat .ge. 6) then
          atm%phase_ssg(iscat_a4,ilay,igeo) = dot_product(gsf_00(0:nleg_lay_cur) , atm%coefs(iscat_a4,0:nleg_lay_cur,ilay))
          atm%phase_ssg(iscat_b2,ilay,igeo) = dot_product(gsf_02(0:nleg_lay_cur) , atm%coefs(iscat_b2,0:nleg_lay_cur,ilay))
        endif

      enddo

    enddo

  end subroutine atmosphere_derive_phase_ssg ! }}}

  subroutine atmosphere_lambert_albedo(nst,nstrhalf,ngeo,execution,lambert_albedo,atm) ! {{{

    implicit none

    ! Input and output.
    integer, intent(in) :: nst !< Number of Stokes parameters.
    integer, intent(in) :: nstrhalf !< Half the number of streams.
    integer, intent(in) :: ngeo !< Number of viewing geometries.
    logical, dimension(3), intent(in) :: execution !< Flags for single, double and multi scattering.
    real, intent(in) :: lambert_albedo !< Lambertian albedo to put in.
    type(lintran_atmosphere), intent(inout) :: atm !< The atmosphere in which to inject the Lambertian albedo.

    ! Highest required Fourier number.
    integer :: nleg

    ! Error handling.
    integer :: ierr

    nleg = 2*nstrhalf - 1 ! No matter if delta-M is used.

    ierr = 0
    if (allocated(atm%bdrf)) deallocate(atm%bdrf)
    if (allocated(atm%bdrf_0)) deallocate(atm%bdrf_0)
    if (allocated(atm%bdrf_v)) deallocate(atm%bdrf_v)
    if (allocated(atm%bdrf_ssg)) deallocate(atm%bdrf_ssg)
    if (execution(2) .or. execution(3)) then
      allocate(atm%bdrf_0(nst,nst,nstrhalf,0:nleg))
      atm%bdrf_0 = 0.0
      atm%bdrf_0(1,1,:,0) = lambert_albedo
      allocate(atm%bdrf_v(nst,nst,nstrhalf,0:nleg,ngeo))
      atm%bdrf_v = 0.0
      atm%bdrf_v(1,1,:,0,:) = lambert_albedo
    endif
    if (execution(3)) then
      allocate(atm%bdrf(nst,nst,nstrhalf,nstrhalf,0:nleg))
      atm%bdrf = 0.0
      atm%bdrf(1,1,:,:,0) = lambert_albedo
    endif
    if (execution(1)) then
      allocate(atm%bdrf_ssg(nst,nst,ngeo))
      atm%bdrf_ssg = 0.0
      atm%bdrf_ssg(1,1,:) = lambert_albedo
    endif

  end subroutine atmosphere_lambert_albedo ! }}}

  subroutine atmosphere_uniform_emissitivy(nst,nstrhalf,ngeo,execution,uniform_emissivity,atm) ! {{{

    implicit none

    ! Input and output.
    integer, intent(in) :: nst !< Number of Stokes parameters.
    integer, intent(in) :: nstrhalf !< Half the number of streams.
    integer, intent(in) :: ngeo !< Number of viewing geometries.
    logical, dimension(3), intent(in) :: execution !< Flags for single, double and multi scattering.
    real, intent(in) :: uniform_emissivity !< Uniform emissivity to put in.
    type(lintran_atmosphere), intent(inout) :: atm !< The atmosphere in which to inject the Lambertian albedo.

    ! Highest required Fourier number.
    integer :: nleg

    ! Error handling.
    integer :: ierr

    nleg = 2*nstrhalf - 1 ! No matter if delta-M is used.

    ierr = 0
    if (allocated(atm%emi)) deallocate(atm%emi)
    if (allocated(atm%emi_ssg)) deallocate(atm%emi_ssg)
    if (execution(2) .or. execution(3)) then
      allocate(atm%emi(nst,nstrhalf,0:nleg))
      atm%emi = 0.0
      atm%emi(1,:,0) = uniform_emissivity
    endif
    if (execution(1)) then
      allocate(atm%emi_ssg(nst,ngeo))
      atm%emi_ssg = 0.0
      atm%emi_ssg(1,:) = uniform_emissivity
    endif

  end subroutine atmosphere_uniform_emissitivy ! }}}

  subroutine check(stat) ! {{{
    
    use netcdf, only: nf90_noerr, nf90_strerror
    
    implicit none

    ! Input and output.
    integer, intent(in) :: stat
    
    if(stat .ne. nf90_noerr) then
      write(*,"(A)") trim(nf90_strerror(stat))
      stop
    endif
        
  end subroutine check ! }}}

end program lintran_standalone
