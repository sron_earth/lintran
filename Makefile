# Choose compilation options:

# parallel = yes / no
parallel = no
# debug = yes / no
debug = no
# FORT = SRON-gfortran / SRON-ifort / KNMI-gfortran
FORT = SRON-ifort

# Directories for source files, object files, modules and dependency files
basedir = $(shell /bin/pwd)/
src = $(basedir)SRC/
mod = $(basedir)MODULES/
obj = $(basedir)OBJECTS/

# Compiler flags
ifeq ($(FORT),SRON-gfortran)
   FC = gfortran
   fortlib = $(NADC_EXTERN)
   FFLAGS = -c -J$(mod) -I$(mod) -I$(fortlib)/include
   FFLAGS += -fimplicit-none -ffree-line-length-none -fdefault-real-8 -fdefault-double-8
   FFLAGS += -fautomatic -fPIC -frecursive
   LDFLAGS = -L$(fortlib)/lib -lnetcdff -lnetcdf -lhdf5_fortran -lhdf5_hl -lhdf5 -lz
   ifeq ($(debug),yes) 
      FFLAGS += -g -fbacktrace -fstack-protector-all 
      FFLAGS += -fcheck=bounds  -fcheck=pointer -fcheck=do -fcheck=all 
      FFLAGS += -ffpe-trap=invalid,zero,overflow
      FFLAGS += -Wunused -Wunused-dummy-argument # -Warray-temporaries
   else 
      FFLAGS += -O3 -funroll-loops #-flto -march=native 
   endif
   ifeq ($(parallel),yes)
      FFLAGS +=  -fopenmp
      LDFLAGS +=  -fopenmp
   endif
else ifeq ($(FORT),SRON-ifort)
   FC = ifort
   FFLAGS =  -c -module $(mod) -I$(mod) -I$(IFC_H5_INC)
   FFLAGS += -implicitnone -132 -fPIC -r8 -auto
   LDFLAGS =  -L$(IFC_H5_LIB) -lnetcdff -lnetcdf -lhdf5_fortran -lhdf5_hl -lhdf5 -lz
   ifeq ($(debug),yes)  
      FFLAGS += -g -traceback -debug full -debug-parameters all 
      FFLAGS += -fpe0 -check bounds -check pointers -check uninit 
      FFLAGS += -check overflow -fp-stack-check -warn unused 
   else
      FFLAGS += -O3 # -xhost -ipo
   endif  
   ifeq ($(parallel),yes)
      FFLAGS +=  -openmp
      LDFLAGS +=  -openmp
   endif
else ifeq ($(FORT),KNMI-gfortran)  
   L2DP_HOME ?= ../tropnll2dp
   include $(L2DP_HOME)/src/Makefile.common
   fortlib = /home/wit/gcc-4.6.3-libs/
   FC = /home/versenda/gcc-4.6.3/bin/g++
   FFLAGS = -c -J$(mod) -I$(mod) -I$(L2DP_HOME)/include
   FFLAGS += -fimplicit-none -ffree-line-length-none -fdefault-real-8 -fdefault-double-8
   FFLAGS += -fstack-protector-all
   FFLAGS += -fautomatic -fPIC
   # Always use -frecursive, to enforce that each thread has its own version of large arrays. Otherwise severe problems may occur
   FFLAGS += -frecursive		
   FFLAGS += -Wuninitialized
   LDFLAGS = -L$(L2DP_HOME)/lib -lnetcdff -lnetcdf -lhdf5_fortran -lhdf5_hl -lhdf5 -lz
   ifeq ($(debug),yes) 
      FFLAGS += -g -fbacktrace -ggdb
      FFLAGS += -fcheck=bounds  -fcheck=pointer -fcheck=do -fcheck=all 
      FFLAGS += -ffpe-trap=invalid,zero,overflow
      FFLAGS += -Wunused -Wunused-dummy-argument
   else 
      FFLAGS += -O2 -funroll-loops
   endif
   # Older setting, apparently not set when using multiple threads
   ifeq ($(parallel),yes)
      FFLAGS +=  -fopenmp -frecursive
      LDFLAGS +=  -fopenmp
   endif
endif

OBJS = \
  $(obj)lintran_constants_module.o \
  $(obj)lintran_types_module.o \
  $(obj)lintran_grid_module.o \
  $(obj)lintran_geometry_module.o \
  $(obj)lintran_pixel_module.o \
  $(obj)lintran_tlc_module.o \
  $(obj)lintran_fields_module.o \
  $(obj)lintran_matrix_module.o \
  $(obj)lintran_interface_module.o \
  $(obj)lintran_nst1_module.o \
  $(obj)lintran_nst3_module.o \
  $(obj)lintran_nst4_module.o \
  $(obj)lintran_module.o \
  $(obj)main.o

run_lintran: $(OBJS) Makefile
	$(FC) $(OBJS) $(LDFLAGS) -o run_lintran 

$(obj)lintran_constants_module.o: $(src)lintran_constants_module.f90 Makefile
	$(FC) $(FFLAGS) -o $(obj)lintran_constants_module.o $(src)lintran_constants_module.f90

$(obj)lintran_types_module.o: $(src)lintran_types_module.f90 Makefile
	$(FC) $(FFLAGS) -o $(obj)lintran_types_module.o $(src)lintran_types_module.f90

$(obj)lintran_grid_module.o: $(src)lintran_grid_module.f90 Makefile \
  $(obj)lintran_constants_module.o
	$(FC) $(FFLAGS) -o $(obj)lintran_grid_module.o $(src)lintran_grid_module.f90

$(obj)lintran_geometry_module.o: $(src)lintran_geometry_module.f90 Makefile \
  $(obj)lintran_constants_module.o \
  $(obj)lintran_grid_module.o
	$(FC) $(FFLAGS) -o $(obj)lintran_geometry_module.o $(src)lintran_geometry_module.f90

$(obj)lintran_pixel_module.o: $(src)lintran_pixel_module.f90 Makefile \
  $(obj)lintran_constants_module.o \
  $(obj)lintran_types_module.o
	$(FC) $(FFLAGS) -o $(obj)lintran_pixel_module.o $(src)lintran_pixel_module.f90

$(obj)lintran_tlc_module.o: $(src)lintran_tlc_module.f90 Makefile \
  $(obj)lintran_constants_module.o \
  $(obj)lintran_grid_module.o \
  $(obj)lintran_geometry_module.o \
  $(obj)lintran_pixel_module.o
	$(FC) $(FFLAGS) -o $(obj)lintran_tlc_module.o $(src)lintran_tlc_module.f90

$(obj)lintran_fields_module.o: $(src)lintran_fields_module.f90 Makefile \
  $(obj)lintran_constants_module.o
	$(FC) $(FFLAGS) -o $(obj)lintran_fields_module.o $(src)lintran_fields_module.f90

$(obj)lintran_matrix_module.o: $(src)lintran_matrix_module.f90 Makefile \
  $(obj)lintran_constants_module.o \
  $(obj)lintran_tlc_module.o
	$(FC) $(FFLAGS) -o $(obj)lintran_matrix_module.o $(src)lintran_matrix_module.f90

$(obj)lintran_interface_module.o: $(src)lintran_interface_module.f90 Makefile \
  $(obj)lintran_constants_module.o \
  $(obj)lintran_tlc_module.o
	$(FC) $(FFLAGS) -o $(obj)lintran_interface_module.o $(src)lintran_interface_module.f90

$(obj)lintran_nst1_module.o: $(src)lintran_nst1_module.f90 Makefile \
  $(src)lintran_transfer_implementation.f90 \
  $(src)lintran_perturbation_scat_implementation.f90 \
  $(src)lintran_perturbation_tau_implementation.f90 \
  $(src)lintran_perturbation_bdrf_implementation.f90 \
  $(src)lintran_perturbation_emi_implementation.f90 \
  $(src)lintran_perturbation_therm_implementation.f90 \
  $(obj)lintran_constants_module.o \
  $(obj)lintran_tlc_module.o
	$(FC) $(FFLAGS) -o $(obj)lintran_nst1_module.o $(src)lintran_nst1_module.f90

$(obj)lintran_nst3_module.o: $(src)lintran_nst3_module.f90 Makefile \
  $(src)lintran_transfer_implementation.f90 \
  $(src)lintran_perturbation_scat_implementation.f90 \
  $(src)lintran_perturbation_tau_implementation.f90 \
  $(src)lintran_perturbation_bdrf_implementation.f90 \
  $(src)lintran_perturbation_emi_implementation.f90 \
  $(src)lintran_perturbation_therm_implementation.f90 \
  $(obj)lintran_constants_module.o \
  $(obj)lintran_tlc_module.o
	$(FC) $(FFLAGS) -o $(obj)lintran_nst3_module.o $(src)lintran_nst3_module.f90

$(obj)lintran_nst4_module.o: $(src)lintran_nst4_module.f90 Makefile \
  $(src)lintran_transfer_implementation.f90 \
  $(src)lintran_perturbation_scat_implementation.f90 \
  $(src)lintran_perturbation_tau_implementation.f90 \
  $(src)lintran_perturbation_bdrf_implementation.f90 \
  $(src)lintran_perturbation_emi_implementation.f90 \
  $(src)lintran_perturbation_therm_implementation.f90 \
  $(obj)lintran_constants_module.o \
  $(obj)lintran_tlc_module.o
	$(FC) $(FFLAGS) -o $(obj)lintran_nst4_module.o $(src)lintran_nst4_module.f90

$(obj)lintran_module.o: $(src)lintran_module.f90 Makefile \
  $(obj)lintran_constants_module.o \
  $(obj)lintran_types_module.o \
  $(obj)lintran_grid_module.o \
  $(obj)lintran_geometry_module.o \
  $(obj)lintran_pixel_module.o \
  $(obj)lintran_tlc_module.o \
  $(obj)lintran_fields_module.o \
  $(obj)lintran_matrix_module.o \
  $(obj)lintran_interface_module.o \
  $(obj)lintran_nst1_module.o \
  $(obj)lintran_nst3_module.o \
  $(obj)lintran_nst4_module.o
	$(FC) $(FFLAGS) -o $(obj)lintran_module.o $(src)lintran_module.f90

$(obj)main.o: $(basedir)main.f90 Makefile \
  $(obj)lintran_constants_module.o \
  $(obj)lintran_types_module.o \
  $(obj)lintran_module.o
	$(FC) $(FFLAGS) -o $(obj)main.o $(basedir)main.f90

clean:
	rm -f $(obj)*.o
	rm -f $(mod)*.mod
	rm -f run_lintran

