from sys import argv
from shutil import copyfile

for el in argv[2:]:
  runidstring = "%4.4i" % int(el)
  copyfile(argv[1],"./settings"+runidstring+".nml")

