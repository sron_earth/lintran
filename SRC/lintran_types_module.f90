!> \file lintran_types_module.f90
!! Module file for all derived types.

!> Module for all derived types used in Lintran.
!!
!! The derived types include the interface types. Internal types are defined inside
!! their respective modules. The user should allocate and define all the contents of the
!! input interface classes and will interpret the output (the derivatives). The only internal
!! class that the user should define is the lintran_class instance, which can be imported
!! from lintran_module. All contents of this lintran_class instance is done by the program,
!! but you can look around in that structure if you are curious.
module lintran_types_module

  implicit none

  !> Structure describing the optical properties of an atmosphere.
  !!
  !! These contents should be provided by the user. It contains the optical properties
  !! of the atmosphere at one wavelength.
  !! <p>The optical properties are given as optical depths and phase coefficients,
  !! so not as wavelength, refractive indices and particle sizes. That means that
  !! the conversion from physical particles to optical properties (e.g. a Mie module)
  !! is not included in Lintran.
  !! <p>The atmosphere is defined from top to bottom. That means that the first layer
  !! defined is the highest in the atmosphere and the last layer ends at the surface.
  !! The surface itself is included in the model atmosphere, with the Bi-directional
  !! reflection function, and fluorescent emissivity.
  type lintran_atmosphere ! {{{

    real :: sun !< Solar irradiance. Make sure to use the same units as the surface emissivity and the planck curve.
    real, dimension(:), allocatable :: taus !< Scattering optical depth. Dimension: Atmospheric layer.
    real, dimension(:), allocatable :: taua !< Absorption optical depth. Dimension: Atmospheric layer.
    real, dimension(:,:,:), allocatable :: coefs !< Phase matrix Legendre coefficients in high definition, which means that Legendre coefficient 1 is in the range \f$ \{-3\dots 3\} \f$, not \f$ \{-1\dots 1\} \f$. First dimension: Independent matrix element, The order is I&#8594;I, I&#8594;Q=Q&#8594;I, Q&#8594;Q, U&#8594;U, V&#8594;U=-U&#8594;V, V&#8594;V. Second dimension: Legendre number (starting at zero). Third dimension: Atmospheric layer. Elements involving non-used Stokes parameters need not be included. At least the required Legendre coefficients must be given. That is \f$ \{0\dots N_s-1\} \f$. Here, \f$ N_s \f$ is the number of streams for which Lintran is initialized. It is possible to provide more Legendre coefficients. They will be ignored. This field need not be allocated for single-scattering.
    real, dimension(:,:,:), allocatable :: phase_ssg !< Scattering matrix at single-scattering geometry. Can be acquired with Legendre coefficients, according to de Haan et al.(1987), equations 68--73. First dimension: Independent matrix element, The order is I&#8594;I, I&#8594;Q=Q&#8594;I, Q&#8594;Q, U&#8594;U, V&#8594;U=-U&#8594;V, V&#8594;V. Second dimension: Atmospheric layer. Third dimension: Viewing geometry. Elements involving non-used Stokes parameters need not be included. This field need to be allocated for multi-scattering.
    real, dimension(:,:,:,:,:), allocatable :: bdrf !< Bi-directional reflection function for internal streams. First dimension: Destination Stokes parameter. Second dimension: Source Stokes parameter. Third dimension: Destination stream. Fourth dimension: Source stream. Fifth dimension: Fourier number (starting at zero). The streams are only defined in one hemisphere, so there are \f$ \frac{N_s}{2} \f$ streams, where \f$ N_s \f$ is the number of streams with which Lintran is initialized. The stream direction cosines can be found in &lt;lintran_instance&gt;%grd%mu when Lintran is initialized. At least Fourier numbers \f$ \{0\dots N_s-1 \} \f$ must be defined (\f$ N_s \f$ is the number of streams with which Lintran is initialized), or when bdrf_only_0 is set to true, only Fourier number 0 must be defined although the Fourier dimension should exist. Defining more Fourier numbers is possible, but they will be ignored. Note that the BDRF must have the correct symmetry relationship. When flipping source and destination streams and Stokes parameters, it should be the same, except when exactly one Stokes parameter is U (3), a minus sign is applied. An example: For Lambertian reflection, all elements for both Stokes parameter I (1) and Fourier index 0 are equal to the albedo, and all other elements are zero. This field need not be allocated for single-scattering.
    real, dimension(:,:,:,:), allocatable :: bdrf_0 !< Bi-directional reflection function with the solar angle. First dimension: Destination Stokes parameter. Second dimension: Source Stokes parameter. Third dimension: Destination stream. Fourth dimension: Fourier number. All properties for the internal BDRF also applies for this BDRF, but there is one internal stream dimension less. Therefore, there is also no longer a symmetry relationship. This field need not be allocated for single-scattering.
    real, dimension(:,:,:,:,:), allocatable :: bdrf_v !< Bi-directional reflection function with the viewing angles. First dimension: Destination Stokes parameter. Second dimension: Source Stokes parameter. Third dimension: Source stream. Fourth dimension: Fourier number. Fifth dimension: Viewing geometry. All properties for the internal BDRF also applies for this BDRF, but there is one internal stream dimension less. Therefore, there is also no longer a symmetry relationship. However, this field has an addition dimension for the viewing geometries. This field need not be allocated for single-scattering.
    real, dimension(:,:,:), allocatable :: bdrf_ssg !< Bi-directional reflection function at single-scattering geometry. This is the BDRF for reflection from the solar direction to the viewing directions, comparable with phase_ssg. First dimension: Destination Stokes parameter. Second dimension: Source Stokes parameter. Third dimension: Viewing geometry. This field need not be allocated for multi-scattering.
    real, dimension(:,:,:), allocatable :: emi !< Surface emissivity. First dimension: Stokes parameter of emitted light. Second dimension: Internal stream. Third dimension: Fourier number. For the Fourier dimension, the same rules apply as for the BDRF. This field need not be allocated for single-scattering.
    real, dimension(:,:), allocatable :: emi_ssg !< Surface emissivity in viewing geometries. First dimension: Stokes parameter of emitted light. Second dimension: Viewing geometry. This field need not be allocated for multi-scattering.
    real, dimension(:), allocatable :: planck_curve !< Position on the Planck curve at the simulated wavelength and the temperature of the atmospheric layer. Make sure to use the same units as the solar irradiance and the surface emissivity. Dimension: Atmospheric layer. This field need not be allocated if thermal emission is turned off.
    integer, dimension(:), allocatable :: nleg_lay !< Highest nonzero Legendre coefficient per layer. Just to save CPU time.
    logical :: bdrf_only_0 !< Flag for that the surface BDRF only has nonzero values for Fourier number 0. Just to save CPU time.
    logical :: emi_only_0 !< Flag for that the fluorescent emissivity only has nonzero values for Fourier number 0. Just to save CPU time.
    logical :: thermal_emission !< Flag for including thermal emission. When turned off, you save CPU time and need not allocate the Planck curve.

  end type lintran_atmosphere ! }}}

  !> Structure for calculation settings.
  !!
  !! This structure contains switches. Most of them trade between speed and precision.
  !! It also contains a selection of which Stokes parameters to calculate and for derivatives,
  !! it contains selection which derivatives should be calculated.
  !! <p>Pseudo-spherical geometry and the selection of single and/or multi
  !! scattering are not included in this structure, but are given as loose parameters when
  !! needed.
  type lintran_settings ! {{{

    ! This structure contains all the switches and settings for running Lintran
    ! It does not include the choice between single-scattering and multi-scattering,
    ! since that is done in two interfaces.
    logical, dimension(:), allocatable :: execute_stokes !< Flags for Stokes parameters to be calculate, for both normal run and derivatives. The order is I, Q, U, V. Non-implemented elements need not be included.
    logical, dimension(:), allocatable :: differentiate_stokes !< Flags for Stokes parameters for which derivatives should be calculated. The order is I, Q, U, V. Non-implemented elements need not be included.
    real :: taus_split !< Maximum scattering optical depth in one sublayer. Layers with higher scattering optical depth are automatically split into multiple sufficiently thin sublayers. Layers are split after eventual application of &delta;-M. Therefore, &delta;-M may reduce the number of sublayers.
    real :: taua_split !< Maximum absorption optical depth in one sublayer. Layers with higher absorption optical depth are automatically split into multiple sufficiently thin sublayers.
    real :: tautot_max !< Maximum total optical depth after which layer-splitting will be disabled. Layers below an optically thick cloud are considered less important, so layer-splitting can be turned off for those layers, increasing the speed. Like for taus_split, this limit is applied after eventual &delta;-M transformation.
    real :: fourier_tolerance !< Ignore higher Fourier numbers when the relative change of the result changes less than this number.
    logical :: split_double !< Switch for solving double-scattering analytically.
    integer :: interpolation !< Interpolation method for intensity inside layer. 0: Averaging. 1: Linear.
    logical :: deltam !< Apply &delta;-M to cut off the forward peak from the phase function.
    integer :: solver !< Choice of which matrix solver to use. 1: Gauss-Seidel. 2: LU-decomposition.
    integer :: gs_maxiter !< Maximum number of iterations for Gauss-Seidel solver. Not needed for LU-decomposition.
    real :: gs_tolerance !< Convergence tolerance for Gauss-Seidel. That is maximum relative change of the downward intensities at the bottom of the atmosphere and the upward intensities at the top of the atmosphere. Not needed for LU-decomposition.
    integer, dimension(:), allocatable :: ilay_deriv_base !< Layer indices for which the derivatives with respect to taus and taua are calculated. Note that layer 1 is at the top of the atmosphere. Need not be allocated when not taking derivatives. Also, leave unallocated if you do not want to differentiate with respect to taus and taua in any layer. Trailing zeros will be ignored, but do not put zeros before nonzero numbers.
    integer, dimension(:), allocatable :: ilay_deriv_ph !< Layer indices for which the derivatives with respect to the phase function is calculated. Note that layer 1 is at the top of the atmosphere. Need not be allocated when not taking derivatives. Also, leave unallocated if you do not want to differentiate with respect to phase functions in any layer. Trailing zeros will be ignored, but do not put zeros before nonzero numbers.
    integer :: nleg_deriv !< Highest Legendre number for which derivatives with respect to phase coefficients are calculated.

  end type lintran_settings ! }}}

  !> Structure for the derivatives.
  !!
  !! The dimensions of all fields are the same as in lintran_atmosphere,
  !! except that the dimensions for atmospheric layers are reduced to the layers
  !! that are selected for calculating the derivatives, and the Legendre coefficicents
  !! in drint_coefs are defined at 0:nleg_deriv, see lintran_settings. Also, each property
  !! gets an additional dimension over viewing Stokes parameters. Furthermore, all fields
  !! that did not yet have a dimension over viewing geometries get one. The viewing geometry
  !! will be the last dimension and the viewing Stokes parameters will be the dimension just
  !! before the one over viewing geometries.
  !! <p>If no layers are selected for differentiation with respect to certain properties
  !! (optical depths or phase functions), then those derivatives also need not be
  !! allocated. Also irrelevant fields, such as the single-scattering phase function when
  !! only calculating multi-scattering need not be allocated (like in lintran_atmosphere).
  !! Fields that are allocated, but did not need to be allocated, will be set to zero.
  type lintran_derivatives ! {{{

    real, dimension(:,:,:), allocatable :: drint_taus !< Derivatives with respect to scattering optical depth. First dimension: Differentiated layer. Second dimension: Viewing Stokes parameter. Third dimension: Viewing geometry.
    real, dimension(:,:,:), allocatable :: drint_taua !< Derivatives with respect to absorption optical depth. First dimension: Differentiated layer. Second dimension: Viewing Stokes parameter. Third dimension: Viewing geometry.
    real, dimension(:,:,:,:,:), allocatable :: drint_coefs !< Derivatives with respect to Legendre coefficients of the phase matrix. First dimension: Independent matrix element, The order is I&#8594;I, I&#8594;Q=Q&#8594;I, Q&#8594;Q, U&#8594;U, V&#8594;U=-U&#8594;V, V&#8594;V. Second dimension: Legendre coefficient. Third dimension: Differentiated layer. Fourth dimension: Viewing Stokes parameter. Fifth dimension: Viewing geometry.
    real, dimension(:,:,:,:), allocatable :: drint_phase_ssg !< Derivatives with respect to the scattering matrix at single-scattering geometries. First dimension: Independent matrix element, The order is I&#8594;I, I&#8594;Q=Q&#8594;I, Q&#8594;Q, U&#8594;U, V&#8594;U=-U&#8594;V, V&#8594;V. Second dimension: Atmospheric layer. Third dimension: Viewing Stokes parameter. Fourth dimension: Viewing geometry.
    real, dimension(:,:,:,:,:,:,:), allocatable :: drint_bdrf !< Derivatives with respect to the bi-directional reflection function for internal streams. First dimension: Destination Stokes parameter of perturbed BDRF element. Second dimension: Source Stokes parameter of perturbed BDRF element. Third dimension: Destination stream of perturbed BDRF element. Fourth dimension: Source stream of perturbed BDRF element. Fifth dimension: Fourier number of perturbed BDRF element. Sixth dimension: Viewing Stokes parameter. Seventh dimension: Viewing geometry.
    real, dimension(:,:,:,:,:,:), allocatable :: drint_bdrf_0 !< Derivatives with respect to the bi-directional reflection function with the solar angle. First dimension: Destination Stokes parameter of perturbed BDRF element. Second dimension: Source Stokes parameter of perturbed BDRF element. Third dimension: Destination stream of perturbed BDRF element. Fourth dimension: Fourier number of perturbed BDRF element. Fifth dimension: Viewing Stokes parameter. Sixth dimension: Viewing geometry.
    real, dimension(:,:,:,:,:,:), allocatable :: drint_bdrf_v !< Derivatives with respect to the bi-directional reflection function with the viewing angle. First dimension: Destination Stokes parameter of perturbed BDRF element. Second dimension: Source Stokes parameter of perturbed BDRF element. Third dimension: Source stream of perturbed BDRF element. Fourth dimension: Fourier number of perturbed BDRF element. Fifth dimension: Viewing Stokes parameter. Sixth dimension: Viewing geometry.
    real, dimension(:,:,:,:), allocatable :: drint_bdrf_ssg !< Derivative with respect to the bi-directional reflection function at single-scattering geometry. First dimension: Destination Stokes parameter of perturbed BDRF element. Second dimension: Source Stokes parameter of perturbed BDRF element. Third dimension: Viewing Stokes parameter. Fourth dimension: Viewing geometry.
    real, dimension(:,:,:,:,:), allocatable :: drint_emi !< Derivative with respect to surface emissivity. First dimension: Stokes parameter of perturbed emissivity. Second dimension: Stream of perturbed emissivity. Third dimension: Fourier number of perturbed emissivity. Fourth dimension: Viewing Stokes parameter. Fifth dimension: Viewing geometry.
    real, dimension(:,:,:), allocatable :: drint_emi_ssg !< Derivative with respect to surface emissivity towards the instrument. First dimension: Stokes parameter of perturbed emissivity. Second dimension: Viewing Stokes parameter. Third dimension: Viewing geometry.
    real, dimension(:,:,:), allocatable :: drint_planck_curve !< Derivative with respect to the normalized position on the Planck curve. First dimension: Atmospheric layer. Second dimension: Viewing Stokes parameter. Third dimension: Viewing geometry.

  end type lintran_derivatives ! }}}

end module lintran_types_module
