!> \file lintran_pixel_module.f90
!! Module file for the optical properties of the atmosphere.

!> Module for the optical properties of the atmosphere.
!!
!! This modules handles everything that does depend on the optical properties of
!! the atmosphere. Those are different for each wavelength, so where the grid
!! and geometry can be recycled for different wavelengths, everything that is
!! handled in this module does change. Unlike lintran_tlc_module, the results
!! of this module are still physical terms, such as optical depth. They are not
!! yet converted into radiative transfer.
module lintran_pixel_module

  use lintran_constants_module, only: errorflag_allocation, errorflag_deallocation, iscat_a1, iscat_b1, iscat_a2, iscat_a3, iscat_b2, iscat_a4, pol_nst, fullpol_nst, lowlimit_tau
  use lintran_types_module, only: lintran_atmosphere, lintran_settings

  implicit none

  !> Structure for atmospheric information of one wavelength pixel.
  !!
  !! This structure contains the atmospheric parameters that can change
  !! for each wavelength. This structure needs to be prepared for each
  !! wavelength. This contains the atmospheric input from lintran_atmosphere
  !! with eventual pre-processing. Also, parameters that need no pre-processing
  !! are copied to this structure, so that the entire pre-processed atmosphere
  !! is present in this structure. A few parameters are added to this structure.
  !! Those are needed only in the main Lintran module. For the core radiative
  !! transfer calculation, the lintran_tlc_class is created.
  type lintran_pixel_class ! {{{

    ! All input parameters that also may depend on the wavelength.
    real :: sun !< Solar irradiance.
    real, dimension(:), allocatable :: tau !< Extinction optical depth after eventual &delta;-M transformation. Dimension: Atmospheric layer.
    real, dimension(:), allocatable :: ssa !< Single-scattering albedo after eventual &delta;-M transformation. Dimension: Atmospheric layer.
    real, dimension(:,:,:), allocatable :: coefs !< Phase Legendre coefficients. First dimension: Independent matrix element, The order is I&#8594;I, I&#8594;Q=Q&#8594;I, Q&#8594;Q, U&#8594;U, V&#8594;U=-U&#8594;V, V&#8594;V. Second dimension: Legendre number. Third dimension: Atmospheric layer.
    real, dimension(:,:,:), allocatable :: phase_ssg !< Phase function at single-scttering geometry. First dimension: Independent matrix element, The order is I&#8594;I, I&#8594;Q=Q&#8594;I, Q&#8594;Q, U&#8594;U, V&#8594;U=-U&#8594;V, V&#8594;V. Second dimension: Atmospheric layer. Third dimension: Viewing geometry.
    real, dimension(:,:,:,:,:), allocatable :: bdrf !< Bi-directional reflection function for internal streams. First dimension: Source Stokes parameter. Second dimension: Destination Stokes parameter. Third dimension: Source stream. Fourth dimension: Destination stream. Fifth dimension: Fourier number.
    real, dimension(:,:,:,:), allocatable :: bdrf_0 !< Bi-directional reflection function in which the solar direction is involved. First dimension: Source Stokes parameter. Second dimension: Destination Stokes parameter. Third dimension: Destination stream. Fourth dimension: Fourier number.
    real, dimension(:,:,:,:,:), allocatable :: bdrf_v !< Bi-directional reflection function in which a viewing direction is involved. First dimension: Source Stokes parameter. Second dimension: Destination Stokes parameter. Third dimension: Source stream. Fourth dimension: Fourier number. Fifth dimension: Viewing geometry
    real, dimension(:,:,:), allocatable :: bdrf_ssg !< Bi-directional reflection function at single-scattering geometry. First dimension: Source Stokes parameter. Second dimension: Destination Stokes parameter. Third dimension: Viewing geometry.
    real, dimension(:,:,:), allocatable :: emi !< Surface emissivity for internal streams, normalized with the solar irradiance. FIrst dimension: Stokes parameter of emitted light. Second dimension: Stream. Thrid dimension: Fourier number.
    real, dimension(:,:), allocatable :: emi_ssg !< Surface emissivity directed towards the instrument, normalized with the solar irradiance. First dimension: Stokes parameter. Second dimension: Viewing geometry.
    real, dimension(:), allocatable :: planck_curve !< Position on the Planck at the simulated wavelength and the temperature of the atmospheric layer. Dimension: Atmospheric layer. This field need not be allocated if thermal emission is turned off.

    ! Information for either-or-not calculating some less convenient stuff.
    integer, dimension(:), allocatable :: nleg_lay !< Highest nonzero Legendre coefficient per layer that is taken into account, so it will never be higher than the number of Legendre coefficients that exist in the calculation.
    logical :: bdrf_only_0 !< Flag for that the surface BDRF only has nonzero values for Fourier number 0.
    logical :: emi_only_0 !< Flag for that the fluorescent emissivity only has nonzero values for Fourier number 0.
    logical :: thermal_emission !< Flag for including thermal emission.

    ! Administrative array.
    integer, dimension(:), allocatable :: nsplit !< Number of sublayers in which a layer is split. Dimension: Atmospheric layer.

    ! Stuff only for derivatives.

    ! Chain rules for derivatives with delta-M.
    real, dimension(:), allocatable :: chain_ssa_ssa !< Chain rule from derivative with respect to preprocessed single-scattering albedo to derivative with respect to raw single-scattering albedo, only non-trivial with &delta;-M. Dimension: Atmospheric layer.
    real, dimension(:), allocatable :: chain_ssa_tau !< Chain rule from derivative with respect to preprocessed optical depth to derivative with respect to raw single-scattering albedo, only non-trivial with &delta;-M. Dimension: Atmospheric layer.
    real, dimension(:), allocatable :: chain_tau_tau !< Chain rule from derivative with respect to preprocessed optical depth to derivative with respect to raw optical depth, only non-trivial with &delta;-M. Dimension: Atmospheric layer.
    real, dimension(:), allocatable :: chain_ph_ph !< Chain rule from derivative with respect to preprocessed phase element to derivative with respect to raw phase element, only non-trivial with &delta;-M. Dimension: Atmospheric layer.

    ! Chain rules for differentiating to the forward peak.
    real, dimension(:), allocatable :: chain_f_ssa !< Chain rule from derivative with respect to preprocessed single-scattering albedo to derivative with respect to the forward-peak fraction, only non-trivial with &delta;-M. Dimension: Atmospheric layer.
    real, dimension(:), allocatable :: chain_f_tau !< Chain rule from derivative with respect to preprocessed optical depth to derivative with respect to the forward-peak fraction, only non-trivial with &delta;-M. Dimension: Atmospheric layer.
    real, dimension(:,:,:), allocatable :: chain_f_ph !< Chain rule from derivative with respect to preprocessed phase element to derivative with respect to the forward-peak fraction, only non-trivial with &delta;-M. First dimension: Independent matrix element. Second dimension: Legendre number. Third dimension: Atmospheric layer.
    real, dimension(:,:,:), allocatable :: chain_f_ph_elem !< Chain rule from derivative with respect to preprocessed phase element in single-scattering geometry to derivative with respect to the forward-peak fraction, only non-trivial with &delta;-M. First dimension: Independent matrix element. Second dimension: Atmospheric layer. Third dimension: Viewing geometry.

    ! Chain rule from scattering zip integrals to scattering parameters or tau.
    real, dimension(:), pointer :: chain_zipscat_c !< Chain rule from scattering zip-integral to a derivative with respect to C, a parameter that looks like \f$ \omega Z \f$. Dimension: Atmospheric layer.
    real, dimension(:), pointer :: chain_zipscat_tau !< Chain rule from scattering zip-integral to a derivative with respect to the optical depth. Dimension: Atmospheric layer.

    ! Chain rule for converting tau/ssa to taus/taua
    ! Chain rule from tau to taus or taua is a trivial one and will not be stored.
    real, dimension(:), allocatable :: chain_taus_ssa !< Chain rule for conversion with respect to the single-scattering albedo to a derivative with respect to scattering optical depth. Dimension: Atmospheric layer.
    real, dimension(:), allocatable :: chain_taua_ssa !< Chain rule for conversion with respect to the single-scattering albedo to a derivative with respect to absorption optical depth. Dimension: Atmospheric layer.

  end type lintran_pixel_class ! }}}

contains

  !> Constructs space for the atmosphere.
  !!
  !! The atmosphere contains the optical properties in a pre-processed,
  !! but they are still optical properties. Also, some chain rule factors
  !! are constructed. Pixel here, stands for one calculation, or one
  !! wavelength pixel.
  subroutine pixel_init(nst,nscat,nstrhalf,nleg,nlay,ngeo,flag_derivatives,pix,stat) ! {{{

    ! Input and output.
    integer, intent(in) :: nst !< Number of Stokes parameters.
    integer, intent(in) :: nscat !< Number of independent matrix elements in phase matrix.
    integer, intent(in) :: nstrhalf !< Half the number of streams.
    integer, intent(in) :: nleg !< Highest Legendre number.
    integer, intent(in) :: nlay !< Number of atmospheric layers.
    integer, intent(in) :: ngeo !< Number of viewing geometries.
    logical, intent(in) :: flag_derivatives !< Flag for possibility of calculating derivatives.
    type(lintran_pixel_class), intent(out), target :: pix !< Internal pixel structure.
    integer, intent(inout) :: stat !< Error code.

    ! Error handling.
    integer :: ierr

    ! Optical properties of the atmosphere, possibly pre-processed.
    allocate( &
      pix%tau(nlay), &
      pix%ssa(nlay), &
      pix%coefs(nscat,0:nleg,nlay), &
      pix%phase_ssg(nscat,nlay,ngeo), &
      pix%bdrf_ssg(nst,nst,ngeo), &
      pix%bdrf_0(nst,nst,nstrhalf,0:nleg), &
      pix%bdrf_v(nst,nst,nstrhalf,0:nleg,ngeo), &
      pix%bdrf(nst,nst,nstrhalf,nstrhalf,0:nleg), &
      pix%emi(nst,nstrhalf,0:nleg), &
      pix%emi_ssg(nst,ngeo), &
      pix%planck_curve(nlay), &
      stat=ierr &
    )
    if (ierr .ne. 0) then
      stat = or(stat,errorflag_allocation)
      return
    endif

    ! Information for either-or-not taking high Legendre numbers into account.
    allocate(pix%nleg_lay(nlay),stat=ierr)
    if (ierr .ne. 0) then
      stat = or(stat,errorflag_allocation)
      return
    endif
    ! Two logicals for restricting surface reflection and emission to Fourier number 0 are scalar.

    ! Layer-splitting.
    allocate(pix%nsplit(nlay),stat=ierr)
    if (ierr .ne. 0) then
      stat = or(stat,errorflag_allocation)
      return
    endif

    if (flag_derivatives) then

      ! Chain rules for delta-M.
      allocate( &
        pix%chain_ssa_ssa(nlay), &
        pix%chain_ssa_tau(nlay), &
        pix%chain_tau_tau(nlay), &
        pix%chain_ph_ph(nlay), &
        stat=ierr &
      )
      if (ierr .ne. 0) then
        stat = or(stat,errorflag_allocation)
        return
      endif
      ! Differentiating with respect to the forward peak.
      allocate( &
        pix%chain_f_ssa(nlay), &
        pix%chain_f_tau(nlay), &
        pix%chain_f_ph(nscat,0:nleg,nlay), &
        pix%chain_f_ph_elem(nscat,nlay,ngeo), &
        stat=ierr &
      )
      if (ierr .ne. 0) then
        stat = or(stat,errorflag_allocation)
        return
      endif

      ! Set pointers that will always remain here. These pointers
      ! are only there to call the same field with a different name.
      pix%chain_zipscat_c => pix%tau ! From scattering zip-integral to C-parameter.
      pix%chain_zipscat_tau => pix%ssa ! From scattering zip-integral to tau.

      ! Chain rules for converting tau/ssa coordintates to taus/taua.
      allocate( &
        pix%chain_taus_ssa(nlay), &
        pix%chain_taua_ssa(nlay), &
        stat=ierr &
      )
      if (ierr .ne. 0) then
        stat = or(stat,errorflag_allocation)
        return
      endif

    endif

  end subroutine pixel_init ! }}}

  !> Constructs the atmosphere and arrays for differentiation chain rules.
  !!
  !! The atmosphere is in principle equal to input, but unused additional
  !! Legendre polynomials or such are cut off. Moreover, with &delta;-M, the
  !! optical properties are adapted so that the forward peak is cut off the
  !! phase function. And the optical depths are converted from absorption and
  !! scattering optical depths to extinction optical depth and single-scattering
  !! albedo.
  subroutine pixel_set(nst,nscat,nstrhalf,nleg,nlay,ngeo,execution,flag_derivatives,atm,set,pix) ! {{{

    ! Input and output.
    integer, intent(in) :: nst !< Number of Stokes parameters.
    integer, intent(in) :: nscat !< Number of independent matrix elements in phase matrix.
    integer, intent(in) :: nstrhalf !< Half the number of streams.
    integer, intent(in) :: nleg !< Highest Legendre number.
    integer, intent(in) :: nlay !< Number of atmospheric layers.
    integer, intent(in) :: ngeo !< Number of viewing geometries.
    logical, dimension(3), intent(in) :: execution !< Flags for executing single, double and multi-scattering, in that order.
    logical, intent(in) :: flag_derivatives !< Flag for calculating derivatives.
    type(lintran_atmosphere), intent(in) :: atm !< Atmosphere provided by the user.
    type(lintran_settings), intent(in) :: set !< Calculation settings provided by the user.
    type(lintran_pixel_class), intent(inout) :: pix !< Internal pixel structure.

    ! Forward peak (for delta-M).
    real, dimension(nlay) :: forward_peak

    ! Optical depth and single-scattering albedo before delta-M.
    real, dimension(nlay) :: tau_predm
    real, dimension(nlay) :: ssa_predm

    ! Iterators.
    integer :: ileg ! Over Legendre coefficients.
    integer :: ilay ! Over atmospheric layers.
    integer :: igeo ! Over viewing geometries.

    ! Apply delta-M correction. See the following papers:
    ! The Delta-M Method: Rapid Yet Accurate Radiative Flux
    ! Calculations to Strongly Asymmetric Phase Functions
    ! W. J. Wiscombe
    ! J. Atmos. Sci.
    ! For a more basic explanation for a two-stream example, read
    ! The delta-Eddington approximation for radiative flux transfer
    ! J. Joseph, W. J. Wiscombe and J. A. Weiman.
    ! J. Atmos. Sci.

    ! The phase function contains elements from 0 to the number of
    ! streams, actually one too many. The last element is for the delta-m
    ! method, where a forward peak is split off from the phase function.
    ! This means that, instead of setting all higher Legendre elements
    ! to zero, they are all set equal to the last number. As no
    ! information about the high Legendre elements is there, this
    ! approximation must be at least as good as truncation (so zero is
    ! at least equally illogical as the last Legendre coefficient). Here,
    ! the last Legendre coefficient is actually the first Legendre
    ! coefficient that would be truncated without the Delta-M method.

    ! In these comments, where we name Legendre coefficients, we mean their
    ! low definition, (with division by 2l+1 or without multiplication by 2l+1).
    ! Though, in the input in the atmosphere structure, the Legendre coefficients
    ! are defined in their high forms.

    ! With the delta-m method, the phase function is split into a
    ! forward delta-peak and an m-Legendre-coefficient phase function.
    ! P = f*delta + (1-f) * M.
    ! in such a way that Legendre elements 0 to m-1 are preserved, and even
    ! Legendre element m is preserved for the upper left corner I to I.
    ! Then, Legendre coefficients m to infinity for all diagonal matrix elements
    ! are set the same, thus equal to element m for I to I.
    ! Then f is equal to Legendre coefficient m in her low definition.
    ! Then, phase function m has coefficients only from 0 to m-1 and
    ! are defined as
    ! X* = (X-f) / (1-f), where X is the original Legendre coefficient in
    ! the low form (and the result X* is also in her low form).
    ! In the high form, we will muliply left and right sides by 2l+1.
    ! X*h = (Xh-(2l+1)f) / (1-f).
    ! The forward-peak of f can be implemented by removing a fraction
    ! f from the scattering cross section.
    ! For off-diagonal terms, the delta contribution is zero, because the
    ! delta is the identity matrix. Those elements will get the transformation
    ! X*h = Xh / (1-f).
    ! The same will happen for the single-scattering phase function.

    ! Calculate f by turning Legendre coefficient m (for I to I) to the low form.
    ! Note that nleg is m-1, so 2*m+1 is 2*nleg+3.

    ! Translate taus and taua to tau and ssa before delta-M.
    tau_predm = atm%taus + atm%taua
    ! Limits to protect from singularies, especially for pseudo-spherical consitions and
    ! for derivatives. Effectively, a layer with too low tau will have an added contribution
    ! of absorption up to lowlimit_tau. Note that atm%taua is not used after this line.
    where (tau_predm .lt. lowlimit_tau)
      tau_predm = lowlimit_tau
    endwhere
    ssa_predm = atm%taus / tau_predm

    ! Save number of Legendre coefficients per layer for usage after delta-M application.
    ! When bothering about delta-M, the original atm%nleg_lay should be used.
    if (execution(2) .or. execution(3)) pix%nleg_lay = min(atm%nleg_lay,nleg) ! Limits to maximum possible Legendre coefficient.

    if (set%deltam) then

      ! Calculate forward peak. It is the low definition of Legendre coefficient nleg+1
      ! Using the original nleg_lay, because we need a Legendre coefficient that will no
      ! longer exist after application of delta-M.
      where (atm%nleg_lay .gt. nleg)
        forward_peak = atm%coefs(iscat_a1,nleg+1,:) / float(2*nleg+3)
      elsewhere
        forward_peak = 0.0
      endwhere

      ! Calculate new phase function by using the formula for the high
      ! forms: X*h = (Xh-(2l+1)f) / (1-f) for the diagonal terms and
      ! X*h = Xh / (1-f) for the non-diagonal terms.
      if (execution(2) .or. execution(3)) then
        do ileg = 0,nleg
          where (pix%nleg_lay .ge. ileg)
            pix%coefs(iscat_a1,ileg,:) = (atm%coefs(iscat_a1,ileg,:) - (float(2*ileg+1)*forward_peak)) / (1.0 - forward_peak)
          endwhere
          if (pol_nst(nst)) then ! Involving Q and U.
            ! Same where structure.
            where (pix%nleg_lay .ge. ileg)
              pix%coefs(iscat_b1,ileg,:) = atm%coefs(iscat_b1,ileg,:) / (1.0 - forward_peak) ! Off-diagonal.
              pix%coefs(iscat_a2,ileg,:) = (atm%coefs(iscat_a2,ileg,:) - (float(2*ileg+1)*forward_peak)) / (1.0 - forward_peak)
              pix%coefs(iscat_a3,ileg,:) = (atm%coefs(iscat_a3,ileg,:) - (float(2*ileg+1)*forward_peak)) / (1.0 - forward_peak)
            endwhere
          endif
          if (fullpol_nst(nst)) then ! Involving V.
            ! Same where structure.
            where (pix%nleg_lay .ge. ileg)
              pix%coefs(iscat_b2,ileg,:) = atm%coefs(iscat_b2,ileg,:) / (1.0 - forward_peak) ! Off-diagonal.
              pix%coefs(iscat_a4,ileg,:) = (atm%coefs(iscat_a4,ileg,:) - (float(2*ileg+1)*forward_peak)) / (1.0 - forward_peak)
            endwhere
          endif
        enddo
      endif

      ! For single-scattering geometry, it is not that important to peel off
      ! the forward peak, because it will not be used. The dividsion by
      ! 1-f is important, because the angle-resolved phase function works
      ! with the same (reduced) scatteriung coefficients. All independent matrix
      ! elements are handled the same.
      if (execution(1)) then
        do igeo = 1,ngeo
          do ilay = 1,nlay
            pix%phase_ssg(:,ilay,igeo) = atm%phase_ssg(1:nscat,ilay,igeo) / (1.0-forward_peak(ilay))
          enddo
        enddo
      endif

      ! Problems may occur at very low solar and viewing angle and very low
      ! aximuthal difference. Then, single-scattering geometry is actually
      ! quite a forward scattering event.

      ! Now use f to adapt the cross sections and the single-scattering albedo.
      pix%tau = tau_predm - forward_peak*atm%taus ! atm%taus = tau_predm * ssa_predm.
      pix%ssa = (1.0-forward_peak)*ssa_predm / (1.0 - forward_peak*ssa_predm)

      ! Fill chain rules. Note that we are using the tau and ssa before
      ! delta-M. Otherwise, it will become a draconian expression.
      if (flag_derivatives) then

        pix%chain_ssa_ssa = (1.0-forward_peak) / (1.0-forward_peak*ssa_predm)**2.0
        pix%chain_ssa_tau = -forward_peak*tau_predm
        pix%chain_tau_tau = 1.0 - forward_peak*ssa_predm

        ! Chain rules for the phase function.
        pix%chain_ph_ph = 1.0 / (1.0-forward_peak)

        ! Some irrelevant numbers can be multiplied with 0.0, so they are indeed irrelevant.
        ! But uninitialized numbers are dangerous, even if they will be multiplied with zero.
        ! Therefore, this zero-initialization is needed, even outside the if-execution clause.
        pix%chain_f_ph = 0.0
        pix%chain_f_ph_elem = 0.0

        if (execution(2) .or. execution(3)) then
          do ileg = 0,nleg
            where (pix%nleg_lay .ge. ileg)
              pix%chain_f_ph(iscat_a1,ileg,:) = (atm%coefs(iscat_a1,ileg,:) - float(2*ileg+1)) / (1.0-forward_peak)**2.0 / float(2*nleg+3)
            endwhere
            if (pol_nst(nst)) then ! Involving Q and U.
              ! Same where structure.
              where (pix%nleg_lay .ge. ileg)
                pix%chain_f_ph(iscat_b1,ileg,:) = atm%coefs(iscat_b1,ileg,:) / (1.0-forward_peak)**2.0 / float(2*nleg+3) ! Off-diagonal.
                pix%chain_f_ph(iscat_a2,ileg,:) = (atm%coefs(iscat_a2,ileg,:) - float(2*ileg+1)) / (1.0-forward_peak)**2.0 / float(2*nleg+3)
                pix%chain_f_ph(iscat_a3,ileg,:) = (atm%coefs(iscat_a3,ileg,:) - float(2*ileg+1)) / (1.0-forward_peak)**2.0 / float(2*nleg+3)
              endwhere
            endif
            if (fullpol_nst(nst)) then ! Involving V.
              ! Same where structure.
              where (pix%nleg_lay .ge. ileg)
                pix%chain_f_ph(iscat_b2,ileg,:) = atm%coefs(iscat_b2,ileg,:) / (1.0-forward_peak)**2.0 / float(2*nleg+3) ! Off-diagonal.
                pix%chain_f_ph(iscat_a4,ileg,:) = (atm%coefs(iscat_a4,ileg,:) - float(2*ileg+1)) / (1.0-forward_peak)**2.0 / float(2*nleg+3)
              endwhere
            endif
          enddo
        endif
        if (execution(1)) then
          do igeo = 1,ngeo
            do ilay = 1,nlay
              pix%chain_f_ph_elem(:,ilay,igeo) = atm%phase_ssg(1:nscat,ilay,igeo) / (1.0-forward_peak(ilay))**2.0 / float(2*nleg+3)
            enddo
          enddo
        endif

        ! Fill chain rules for differentiating with respect to the forward peak
        ! Note the division by 2*nleg+3 for the conversion between low and high
        ! definition of Legendre coefficient nleg+1.
        pix%chain_f_ssa = (ssa_predm*(ssa_predm-1.0)) / (1.0 - forward_peak*ssa_predm)**2.0 / float(2*nleg+3)
        pix%chain_f_tau = -atm%taus / float(2*nleg+3) ! atm%taus = tau_predm * ssa_predm.

      endif

    else

      ! Ignore the a possible phase coefficient nleg+1.
      if (execution(2) .or. execution(3)) then
        do ilay = 1,nlay
          pix%coefs(:,0:pix%nleg_lay(ilay),ilay) = atm%coefs(1:nscat,0:pix%nleg_lay(ilay),ilay)
        enddo
      endif
      ! Leave tau, ssa and angle-resolved phase function the same.
      pix%tau = tau_predm
      pix%ssa = ssa_predm
      if (execution(1)) pix%phase_ssg = atm%phase_ssg(1:nscat,:,1:ngeo)

    endif

    ! Conversion coordinates tau/ssa to taus/taua, always needed for derivatives
    ! independent of delta-M. These chain rules are applied when delta-M is fully
    ! closed.
    if (flag_derivatives) then
      ! Due to the limit of tau approaching zero, we will not use atm%taua.
      pix%chain_taus_ssa = (tau_predm - atm%taus) / tau_predm**2.0
      pix%chain_taua_ssa = -atm%taus / tau_predm**2.0
    endif

    ! First copy flags for restriction to Fourier number 0.
    pix%bdrf_only_0 = atm%bdrf_only_0
    pix%emi_only_0 = atm%emi_only_0

    ! Copy information that needs no pre-processing.
    pix%sun = atm%sun
    if (execution(3)) then
      if (pix%bdrf_only_0) then
        pix%bdrf(:,:,:,:,0) = atm%bdrf(1:nst,1:nst,:,:,0)
        ! Transpose the matrix, so that source is before destination.
        pix%bdrf(:,:,:,:,0) = reshape(pix%bdrf(:,:,:,:,0) , (/nst,nst,nstrhalf,nstrhalf/), order = (/2,1,4,3/))
      else
        pix%bdrf = atm%bdrf(1:nst,1:nst,:,:,0:nleg)
        ! Transpose the matrix, so that source is before destination.
        pix%bdrf = reshape(pix%bdrf , (/nst,nst,nstrhalf,nstrhalf,nleg+1/), order = (/2,1,4,3,5/))
      endif
    endif
    if (execution(2) .or. execution(3)) then
      if (pix%bdrf_only_0) then
        pix%bdrf_0(:,:,:,0) = atm%bdrf_0(1:nst,1:nst,:,0)
        pix%bdrf_v(:,:,:,0,:) = atm%bdrf_v(1:nst,1:nst,:,0,1:ngeo)
        ! Transpose the matrices, so that source is before destination.
        pix%bdrf_0(:,:,:,0) = reshape(pix%bdrf_0(:,:,:,0) , (/nst,nst,nstrhalf/), order = (/2,1,3/))
        pix%bdrf_v(:,:,:,0,:) = reshape(pix%bdrf_v(:,:,:,0,:) , (/nst,nst,nstrhalf,ngeo/), order = (/2,1,3,4/))
      else
        pix%bdrf_0 = atm%bdrf_0(1:nst,1:nst,:,0:nleg)
        pix%bdrf_v = atm%bdrf_v(1:nst,1:nst,:,0:nleg,1:ngeo)
        ! Transpose the matrices, so that source is before destination.
        pix%bdrf_0 = reshape(pix%bdrf_0 , (/nst,nst,nstrhalf,nleg+1/), order = (/2,1,3,4/))
        pix%bdrf_v = reshape(pix%bdrf_v , (/nst,nst,nstrhalf,nleg+1,ngeo/), order = (/2,1,3,4,5/))
      endif

      if (pix%emi_only_0) then
        pix%emi(:,:,0) = atm%emi(1:nst,:,0)
      else
        pix%emi = atm%emi(1:nst,:,0:nleg)
      endif
    endif
    if (execution(1)) then
      pix%bdrf_ssg = atm%bdrf_ssg(1:nst,1:nst,1:ngeo)
      ! Transpose the matrix, so that source is before destination.
      pix%bdrf_ssg = reshape(pix%bdrf_ssg , (/nst,nst,ngeo/), order = (/2,1,3/))
      pix%emi_ssg = atm%emi_ssg(1:nst,1:ngeo)
    endif
    ! Planck is needed for any execution: 1, 2 or 3. But a flag is needed to turn it on.
    pix%thermal_emission = atm%thermal_emission
    if (pix%thermal_emission) pix%planck_curve = atm%planck_curve

    ! Calculate layer-splitting.
    if (execution(3)) then
      pix%nsplit = max(1,ceiling(pix%tau*pix%ssa/set%taus_split),ceiling(pix%tau*(1.0-pix%ssa)/set%taua_split)) ! This is after delta-M correction.
      ! The atmosphere below a thick cloud does not matter. The layers below such a
      ! thick cloud will not be split and therfore will not cause much additional
      ! computational burden.
      do ilay = nlay-1,1,-1
        if (sum(pix%tau(1:ilay)) .lt. set%tautot_max) then
          ! Atmosphere down to layer ilay+1 must be split, from ilay+2, it need not be split.
          pix%nsplit(ilay+2:nlay) = 1 ! This will do nothing in most cases, when ilay+2 is larger than nlay.
          exit ! Breaks the loop, because the border of tautot_max has been found.
        endif
      enddo
    else
      ! To avoid problems with, for example, stack allocations, we initialize the number of
      ! split layers to what it actually is when there is no split environment.
      pix%nsplit = 1
    endif

  end subroutine pixel_set ! }}}

  !> Cleans up the pixel.
  !!
  !! All arrays that are allocated in pixel_init are cleaned up.
  subroutine pixel_close(flag_derivatives,pix,stat) ! {{{

    ! Input and output.
    logical, intent(in) :: flag_derivatives !< Flag tor whether Lintran was initialized for derivatives.
    type(lintran_pixel_class), intent(inout) :: pix !< Internal pixel structure to be deconstructed.
    integer, intent(inout) :: stat !< Error code.

    ! Error handling.
    integer :: ierr

    if (flag_derivatives) then
      deallocate( &
        pix%chain_taua_ssa, &
        pix%chain_taus_ssa, &
        pix%chain_f_ph_elem, &
        pix%chain_f_ph, &
        pix%chain_f_tau, &
        pix%chain_f_ssa, &
        pix%chain_ph_ph, &
        pix%chain_tau_tau, &
        pix%chain_ssa_tau, &
        pix%chain_ssa_ssa, &
        stat=ierr &
      )
      if (ierr .ne. 0) stat = or(stat,errorflag_deallocation)
    endif
    deallocate( &
      pix%nsplit, &
      pix%nleg_lay, &
      pix%planck_curve, &
      pix%emi_ssg, &
      pix%emi, &
      pix%bdrf, &
      pix%bdrf_v, &
      pix%bdrf_0, &
      pix%bdrf_ssg, &
      pix%phase_ssg, &
      pix%coefs, &
      pix%ssa, &
      pix%tau, &
      stat=ierr &
    )
    if (ierr .ne. 0) stat = or(stat,errorflag_deallocation)

  end subroutine pixel_close ! }}}

end module lintran_pixel_module
