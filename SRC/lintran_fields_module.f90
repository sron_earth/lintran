!> \file lintran_fields_module.f90
!! Module file for intensity fields.

!> Module for intensity fields.
!!
!! This modules constructs space for intensity fields. There are analytic fields
!! and interpolated fields. The latter benefit from layer splitting and therefore
!! have a varying spatial dimension size. Those fields will only be allocated during
!! the calculation and not during the initialization. These are the only arrays that are
!! allocated and deallocated during the calculation.
module lintran_fields_module

  use lintran_constants_module, only: errorflag_allocation, errorflag_deallocation

  implicit none

  !> Structure for intensity fields.
  !!
  !! This structure contains some intensity fields to work with. These
  !! can be the source, the response function and an intensity field.
  !! Because stuff is never as easy as it looks, it contains several
  !! fields where permanent or temporary stuff can be dumped. This
  !! stuff can vary to such an extent that it is not worthwhile to give
  !! meaningful names to the fields. In lintran_module, where the members
  !! of this structure are explicitly referred to, comments are used to
  !! indicate what is what.
  !! <p>Some arrays are analytic and contain a fixed number of atmospheric
  !! layers. Some are interpolated and benefit from layer splitting and
  !! therefore, the fields can grow larger or smaller. Only the analytic fields
  !! are included in this structure. The interpolated fields are stored in a
  !! different structure, which is not kept allocated between different Lintran runs.
  !! <p> All fields are four-dimensional. First dimension: Stokes parameter.
  !! Second dimension: Stream. Third dimension: down or up (0 or 1). Fourth
  !! dimension: interface (0 to number of layers).
  !! However, for the matrix, the Stokes and stream dimensions are combined,
  !! because some compilers do not allow eight dimensions. Therefore, the matrix
  !! shifts are also set up in that way to stay consistent with the matrix.
  type lintran_fields_class ! {{{

    ! Analytic fields.
    real, dimension(:,:,:,:), allocatable :: a1 !< Analytic field 1.
    real, dimension(:,:,:,:), allocatable :: a2 !< Analytic field 2.
    real, dimension(:,:,:,:), allocatable :: a3 !< Analytic field 3. Only used with derivatives.

  end type lintran_fields_class ! }}}

  !> Structure for dynamic intensity fields.
  !!
  !! This structure contains similar fields as the lintran_fields_class, but these are
  !! dynamic in size and will not be kept allocated after the run. Where most of the content
  !! can be kept allocated because the sizes stay the same, fields subject to layer splitting
  !! are useless after a run and therefore only exist at the scope inside Lintran.
  type, public :: lintran_dynamic_fields_class ! {{{

    ! Interpolated fields.
    real, dimension(:,:,:,:), allocatable :: i1 !< Interpolated field 1.
    real, dimension(:,:,:,:), allocatable :: i2 !< Interpolated field 2.
    real, dimension(:,:,:,:), allocatable :: i3 !< Interpolated field 3. Only used with derivatives.
    real, dimension(:,:,:,:), allocatable :: i4 !< Interpolated field 4. Only used with derivatives.

    ! The matrix, in eight-dimensional band structure, where dimensions 1 and 2 and
    ! dimensions 5 and 6 are merged. But the physical meaning is eight dimensions.
    ! First four dimensions: Source, where the fourth dimension is only
    ! 0:1, for the two relevant layers
    ! Last four dimensions: Destination in full (0:nsplit_total) representation.
    real, dimension(:,:,:,:,:,:), allocatable :: mat !< The matrix. First three dimensions: The source, where the Stokes and steam dimensions are meged. Last three dimensions: The destination, where also the Stokes and stream dimensions are merged. For the source, the layers are reduced to just 0 and 1.

    ! Matrix shifts (for use of banded matrix).
    integer, dimension(:,:,:), allocatable :: shifts !< Because in the matrix, the source layer dimension is reduced to a 0--1 domain, simply reshaping it to two-dimensional does not yet give a two-dimensional matrix. For each destiation index, only a limited number of source indices are defined. The square matrix is applied by writing a number of zeros, then the defined values and continue with zeros again. The number of zeros to write at the beginning is stored in this array. The dimensions stand for the destination index, but it is defined in the original four-dimensional destination (Stokes, stream, down/up, interface). Similarly to the matrix, the Stokes and stream dimensions are combined. This is not necessary, but it is just to stay consistent with the matrix, the closest relative of this shifts array.

  end type lintran_dynamic_fields_class ! }}}

contains

  !> Construct space needed for fixed-sized intensity fields.
  !!
  !! Because this is still the initialization phase, it is not yet known to what extent
  !! layers will be split, so only those without layer splitting are allocated. When Lintran is
  !! not initialized to calculate derivatives, fewer fields will be needed and therefore
  !! fewer fields will be allocated.
  subroutine fields_init(nst,nstrhalf,nlay,flag_derivatives,fld,stat) ! {{{

    ! Input and output.
    integer, intent(in) :: nst !< Number of Stokes parameters.
    integer, intent(in) :: nstrhalf !< Half the number of streams.
    integer, intent(in) :: nlay !< Number of atmospheric layers.
    logical, intent(in) :: flag_derivatives !< Flag for possibility of calculating derivatives.
    type(lintran_fields_class), intent(out) :: fld !< Internal fields structure.
    integer, intent(inout) :: stat !< Error code.

    ! Error handling.
    integer :: ierr

    ! Allocate analytic fields, 2 necessary without differentiating.
    allocate( &
      fld%a1(nst,nstrhalf,0:1,0:nlay), &
      fld%a2(nst,nstrhalf,0:1,0:nlay), &
      stat=ierr &
    )
    if (ierr .ne. 0) then
      stat = or(stat,errorflag_allocation)
      return
    endif

    ! Additional fields for derivatives.
    if (flag_derivatives) then
      ! 1 additional analytic field makes 3.
      allocate(fld%a3(nst,nstrhalf,0:1,0:nlay),stat=ierr)
      if (ierr .ne. 0) then
        stat = or(stat,errorflag_allocation)
        return
      endif
    endif

  end subroutine fields_init ! }}}

  !> Construct space needed for dynamic-sized intensity fields.
  !!
  !! Because every calculation can have a different layer splitting, the number of layers and
  !! therefore, the field sizes can change. Therefore, these are allocated during the calculation
  !! and not during the initialization. When derivatives are not calculated, fewer fields will be
  !! needed and therefore fewer fields will be allocated. Fewer fields are also allocated when
  !! Lintran was initialized for derivatives, but a non-derivatives run is performed.
  subroutine fields_dynamic_init(nst,nstrhalf,nsplit_total,flag_derivatives,dyn,stat) ! {{{

    ! Input and output.
    integer, intent(in) :: nst !< Number of Stokes parameters.
    integer, intent(in) :: nstrhalf !< Half the number of streams.
    integer, intent(in) :: nsplit_total !< Total number of sublayers.
    logical, intent(in) :: flag_derivatives !< Flag for possibility of calculating derivatives.
    type(lintran_dynamic_fields_class), intent(inout) :: dyn !< Non-internal structure for fields with changing size.
    integer, intent(inout) :: stat !< Error code.

    ! Error handling.
    integer :: ierr

    ! Interpolated fields, 2 necessary without differentiating.
    ! For the matrix and the shifts, Stokes and stream dimensions are combined. Otherwise,
    ! gfortran cannot handle the number of dimensions. The reason that dimensions are
    ! combined for the shifts as well is to make them as similar to the matrix as possible.
    ! It is the matrix that has the many dimensions.
    allocate( &
      dyn%i1(nst,nstrhalf,0:1,0:nsplit_total), &
      dyn%i2(nst,nstrhalf,0:1,0:nsplit_total), &
      dyn%mat(nst*nstrhalf,0:1,0:1,nst*nstrhalf,0:1,0:nsplit_total), &
      dyn%shifts(nst*nstrhalf,0:1,0:nsplit_total), &
      stat=ierr &
    )
    if (ierr .ne. 0) then
      stat = or(stat,errorflag_allocation)
      return
    endif

    ! Additional fields for derivatives.
    if (flag_derivatives) then
      ! 2 additional interpolated fields makes 4.
      allocate( &
        dyn%i3(nst,nstrhalf,0:1,0:nsplit_total), &
        dyn%i4(nst,nstrhalf,0:1,0:nsplit_total), &
        stat=ierr &
      )
      if (ierr .ne. 0) then
        stat = or(stat,errorflag_allocation)
        return
      endif
    endif

  end subroutine fields_dynamic_init ! }}}

  !> Cleans up the fixed-sized fields.
  !!
  !! The static fields are member of the lintran instance, which exists on a scope outside
  !! Lintran. Therefore, a closing routine must be provided, because Lintran cannot know if
  !! a user wants to deconstruct and reconstruct a Lintran before it leaves the scope.
  subroutine fields_close(flag_derivatives,fld,stat) ! {{{

    ! Input and output.
    logical, intent(in) :: flag_derivatives !< Flag tor whether Lintran was initialized for derivatives.
    type(lintran_fields_class), intent(inout) :: fld !< Internal pixel structure to be deconstructed.
    integer, intent(inout) :: stat !< Error code.

    ! Error handling.
    integer :: ierr

    if (flag_derivatives) then
      deallocate(fld%a3,stat=ierr)
      if (ierr .ne. 0) stat = or(stat,errorflag_deallocation)
    endif
    deallocate( &
      fld%a2, &
      fld%a1, &
      stat=ierr &
    )
    if (ierr .ne. 0) stat = or(stat,errorflag_deallocation)

  end subroutine fields_close ! }}}

end module lintran_fields_module
